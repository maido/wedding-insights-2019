/**
 * @prettier
 */
import fs from 'fs';
import React from 'react';
import ejs from 'ejs';
import get from 'lodash/get';
import {renderToString} from 'react-dom/server';
import {minify} from 'html-minifier';
import App from './src/js/containers/App';

const getJSFile = () => {
    return new Promise((resolve, reject) => {
        fs.readdir('./dist/static/js/', (err, files = []) => {
            if (err) {
                reject(err);
            }

            const jsFiles = files.filter(f => f.includes('.js'));

            resolve(jsFiles);
        });
    });
};

const init = () => {
    fs.readFile(`./views/index.ejs`, 'utf8', (err, ejsTemplate) => {
        fs.readFile(`./src/content/en.json`, 'utf8', (err, content) => {
            const parsedContent = JSON.parse(content);
            const appHTML = renderToString(<App content={parsedContent} />);
            const meta = get(parsedContent, 'meta');
            const heroImage = get(parsedContent, 'header.hero.photo.small');
            const template = ejs.compile(ejsTemplate, {rmWhitespace: true});

            getJSFile()
                .then(jsFiles => {
                    fs.writeFile(
                        `./dist/index.html`,
                        minify(
                            template({
                                appHTML,
                                heroImage,
                                jsFiles,
                                meta,
                                content: JSON.stringify(parsedContent)
                            }),
                            {collapseWhitespace: true}
                        ),
                        err => {
                            if (err) console.log(err);

                            console.log('saved!');
                        }
                    );
                })
                .catch(boo => console.log('error compiling for ' + file));
        });
    });
};

init();
