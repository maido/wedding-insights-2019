/**
 * @prettier
 * @flow
 */
import brownLightWoff from './brown-light.woff';
import brownLightWoff2 from './brown-light.woff2';
import brownRegularWoff from './brown-regular.woff';
import brownRegularWoff2 from './brown-regular.woff2';
import chupadaDemiboldWoff from './chupada-demibold.woff';
import chupadaDemiboldWoff2 from './chupada-demibold.woff2';

export {
    brownLightWoff,
    brownLightWoff2,
    brownRegularWoff,
    brownRegularWoff2,
    chupadaDemiboldWoff,
    chupadaDemiboldWoff2
};
