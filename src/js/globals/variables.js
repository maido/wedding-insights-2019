/**
 * @prettier
 * @flow
 */
export const breakpoints = {
    mobileSmall: 540,
    mobile: 741,
    tabletSmall: 769,
    tablet: 1025,
    desktop: 1300
};

export const colors = {
    white: '#fdfaf6',
    black: '#2F3745',
    grey: '#9B9B9B',
    greyLight: '#E5E6E8',
    greyDark: '#3D4451',
    greyMedium: '#D8DADC',
    merino: '#F3EDDD',
    red: '#F6423B',
    wewak: '#f5a0a8',
    tiber: '#07392D',
    bianca: '#fdfaf6',

    imagePlaceholder: '#e5e5e6',

    primary: '#07392d',
    secondary: '#f5a0a8',
    tertiary: '#fdfaf6'
};

export const themes = {
    primary: {
        accent: colors.wewak,
        background: colors.tiber,
        heading: colors.wewak,
        link: colors.white,
        text: colors.white
    },
    secondary: {
        accent: colors.tiber,
        background: colors.wewak,
        heading: colors.tiber,
        link: colors.tiber,
        text: colors.white
    },
    tertiary: {
        accent: colors.wewak,
        background: colors.bianca,
        heading: colors.wewak,
        link: colors.tiber,
        text: colors.tiber
    },
    light: {
        accent: colors.wewak,
        background: '#fff',
        heading: colors.wewak,
        link: colors.wewak,
        text: colors.tiber
    }
};

export const fontFamilies = {
    default: "'Brown Light', Arial",
    bold: "'Brown Regular', Arial",
    light: "'Brown Light', Arial",
    heading: "'Chupada Demibold', Arial"
};

export const fontSizes = {
    default: 18,
    lead: 20,
    h1: 120,
    h2: 90,
    h3: 33,
    h4: 27,
    h5: 16,
    h6: 12
};

export const radius = 0;

export const spacing = {
    xs: 6,
    s: 12,
    m: 24,
    l: 48,
    xl: 72,
    grid: 120,
    contentType: 250,
    none: 0
};

export const transitions = {
    default: 'all .2s ease-in-out',
    slow: 'all .3s ease-out',
    bezier: 'all .25s cubic-bezier(.23,1,.32,1)',
    bezierSlow: 'all .85s cubic-bezier(.23,1,.32,1)'
};
