/**
 * @prettier
 * @flow
 */
import {rem} from 'polished';

export const responsiveSpacing = (unit: number = 0, calculation: string = '+') => {
    if (unit > 0) {
        return `calc(${rem(unit)} ${calculation} var(--responsive-spacing))`;
    } else {
        return unit;
    }
};

export const photoURL = (filename: string = '', folder: string = 'content') =>
    filename.includes('http') ? filename : `/static/images/${folder && `${folder}/`}${filename}`;
