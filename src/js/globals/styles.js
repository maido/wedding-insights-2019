/**
 * @prettier
 * @flow
 */
import {css} from '@emotion/core';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from './variables';
// import * as fonts from '../../fonts';

export const styles = css`
    :root {
        --responsive-spacing: 0.1px;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        :root {
            --responsive-spacing: ${rem(spacing.s)};
        }
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        :root {
            --responsive-spacing: ${rem(spacing.m)};
        }
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit;
    }

    html {
        background-color: ${colors.white};
        box-sizing: border-box;
        color: ${colors.black};
        font-family: ${fontFamilies.default};
        overflow-x: hidden;
        -moz-osx-font-smoothing: grayscale;
        -webkit-font-smoothing: antialiased;
        -webkit-text-size-adjust: 100%;
    }

    body {
        font-size: 1em;
        line-height: 1.5;
        margin: 0;
        max-width: 100%;
        overflow-x: hidden;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        body {
            font-size: 0.95em;
        }
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        body {
            font-size: 1.1em;
        }
    }

    button {
        background: none;
        border: 0;
        cursor: pointer;
        outline: none;
        padding: 0;
    }

    i,
    em,
    b,
    strong {
        font-family: ${fontFamilies.default};
    }

    img {
        max-width: 100%;
    }

    ::selection {
        background-color: ${colors.primary};
        color: ${colors.white};
    }

    a,
    button {
        color: inherit;
        text-decoration: none;
        transition: ${transitions.default};
    }

    a {
        color: ${colors.primary};
    }
    a:hover,
    a:focus {
        color: ${colors.greyDark};
    }

    p {
        margin: 0;
    }

    p + * {
        margin-top: ${rem(spacing.m)};
    }

    @media (prefers-reduced-motion: reduce) {
        * {
            animation: none !important;
        }
    }

    @supports (object-fit: cover) {
        @media (max-width: ${rem(breakpoints.mobile)}) {
            img[data-wide],
            [data-wide] img {
                height: 300px;
                object-fit: cover;
            }
        }
    }

    @font-face {
        font-family: 'Brown Light';
        src: local('Brown Light'), url('/static/fonts/brown-light.woff2') format('woff2'),
            url('/static/fonts/brown-light.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: 'Brown Regular';
        src: local('Brown Regular'), url('/static/fonts/brown-regular.woff2') format('woff2'),
            url('/static/fonts/brown-regular.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }

    @font-face {
        font-family: 'Chupada Demibold';
        src: local('Chupada Demibold'), url('/static/fonts/chupada-demibold.woff2') format('woff2'),
            url('/static/fonts/chupada-demibold.woff') format('woff');
        font-weight: normal;
        font-style: normal;
    }
`;
