/**
 * @prettier
 * @flow
 */
import React from 'react';
import Container from '../Container';
import * as S from './styles';

const LinkBanner = ({text, cta}: TextBanner) => (
    <S.Wrapper href={cta.url} rel="noopener">
        <Container verticalPadding={false}>
            {text}
            <S.Domain>{cta.label}</S.Domain>
        </Container>
    </S.Wrapper>
);

export default LinkBanner;
