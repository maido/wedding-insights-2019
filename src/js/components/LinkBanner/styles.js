/**
 * @prettier
 * @flow
 */
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

const entranceAnimation = keyframes`
    from {
        opacity: 0;
        transform: translateY(-200px)
    }
    to {
        opacity: 1;
        transform: translateY(0)
    }
`;

export const Wrapper = styled.section`
    animation: ${entranceAnimation} 1s cubic-bezier(0.23, 1, 0.32, 1);
    animation-fill-mode: forwards;
    animation-delay: 1s;
    background-color: ${colors.tertiary};
    background-image: url('/static/images/noise-texture.png');
    background-size: 100px 100px;
    border-bottom: 1px solid ${colors.greyLight};
    cursor: pointer;
    font-size: ${rem(12)};
    font-family: ${fontFamilies.bold};
    opacity: 0;
    padding: ${rem(spacing.xs)};
    position: relative;
    transition: ${transitions.default};
    transform: translateY(-200px);
    z-index: 200;

    &:hover,
    &:focus {
        background-color: ${colors.wewak};
        color: ${colors.tiber};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(14)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(11)};

        > div {
            padding-left: ${rem(spacing.s)};
            padding-right: ${rem(spacing.s)};
        }
    }
`;

export const Domain = styled.a`
    color: ${colors.wewak};
    font-family: ${fontFamilies.bold};

    &::before {
        color: ${colors.tiber};
        content: '—';
        margin: 0 ${rem(spacing.xs)};
    }

    ${Wrapper}:hover &,
    ${Wrapper}:focus & {
        color: ${colors.white};
    }
`;
