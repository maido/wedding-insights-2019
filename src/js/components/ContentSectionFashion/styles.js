/**
 * @prettier
 */
import styled from '@emotion/styled';
import {between, cover, rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, fontFamilies, spacing, themes, transitions} from '../../globals/variables';

const theme = themes.secondary;

export const Wrapper = styled.section`
    color: ${theme.text};
    font-family: ${fontFamilies.bold};
    padding: ${responsiveSpacing(200)} 0;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        padding: ${responsiveSpacing(80)} 0;

        .no-js & {
            background-color: ${theme.background};
            background-image: url('/static/images/noise-texture.png');
            background-size: 100px 100px;
        }

        [data-text-image] {
            display: flex !important;
            flex-direction: column;

            [data-text] {
                order: 2;
            }
            [data-image] {
                order: 1;
            }
        }
    }
`;

export const FeaturedInstagramAccountContainer = styled.div`
    bottom: ${rem(spacing.xl)};
    position: absolute;
    right: ${rem(spacing.grid * 0.85 * -1)};
    max-width: 475px;
    width: calc(100% - ${rem(spacing.grid * 0.85)});
    z-index: 11;
`;

export const Label = styled.span`
    display: block;
    color: ${theme.accent};
    font-size: ${rem(14)};
    font-weight: 800;
    letter-spacing: ${rem(1)};
    margin-bottom: ${rem(spacing.m)};
    text-transform: uppercase;
`;

export const Heading = styled.h4`
    font-family: ${fontFamilies.heading};
    font-size: ${between('60px', '73px')};
    font-weight: 300;
    line-height: 0.95;
    margin-bottom: ${rem(spacing.s)};
    margin-top: 0;
    text-transform: uppercase;
`;

export const Text = styled.p`
    a {
        color: ${theme.accent};
        font-weight: 600;
        position: relative;
    }
    a::after {
        background-color: ${theme.text};
        bottom: -3px;
        content: '';
        height: 2px;
        left: 0;
        opacity: 0;
        position: absolute;
        transition: ${transitions.default};
        width: 0;
    }

    a:hover,
    a:focus {
        color: ${theme.text};
    }
    a:hover::after,
    a:focus::after {
        opacity: 1;
        width: 100%;
    }
`;

export const IllustrationDoves = styled.div`
    img {
        margin-left: ${rem(-100)};
        margin-top: ${rem(150)};
        max-width: calc(100% + ${rem(75)});
        position: relative;
        width: calc(100% + ${rem(75)});
        z-index: 2;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        img {
            margin-left: 50%;
            margin-top: 0;
            width: 230px;
        }
    }
`;

export const IllustrationRoseContainer = styled.div`
    height: 0;
    padding-bottom: 50%;
    position: relative;
    right: -30%;
    top: -5%;
    width: 450px !important;
    z-index: 11;

    .parallax-outer,
    .parallax-inner {
        height: 100%;
        left: 0;
        right: 0;
        position: absolute;
    }
`;

export const IllustrationRose = styled.div`
    img {
        ${cover()};
        z-index: 11;
    }
`;

export const IllustrationFlowers = styled.div`
    img {
        bottom: 8%;
        min-height: 0 !important;
        position: absolute;
        right: -6%;
        width: 350px !important;
        z-index: 11;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        img {
            right: -20%;
            top: 40%;
            width: 50% !important;
        }
    }
`;

export const IllustrationWreath = styled.div`
    position: absolute;
    left: 80%;
    margin-top: -170%;
    width: 70%;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        left: -10%;
        margin-top: 5%;
        width: 100%;
    }
`;

export const Pattern1 = styled.div`
    background-image: url('/static/images/line-pattern.png');
    background-size: auto auto;
    background-repeat: no-repeat;
    height: 0;
    left: ${rem(spacing.l * -1)};
    position: absolute;
    padding-bottom: 40%;
    top: ${rem(spacing.l * -1)};
    width: 100%;
    z-index: 1;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        background-size: 400px auto;
        top: calc(75px + ${rem(spacing.m * -1)});

        & + div {
            margin-top: 75px;
        }
    }
`;

export const Pattern2 = styled(Pattern1)`
    bottom: ${rem(spacing.m * -1)};
    top: auto;
`;

export const Pattern3 = styled(Pattern1)`
    bottom: ${rem(spacing.m * -1)};
    left: auto;
    right: ${rem(spacing.m * -1)};
    top: auto;
`;

export const StatContainer = styled.div`
    left: 50%;
    max-width: 300px;
    position: absolute;
    top: 60%;
    transform: translateX(-50%);
    z-index: 10;
`;

export const StatContainerHorizontal = styled.div`
    margin-left: 10%;
    max-width: 80%;
    z-index: 10;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        bottom: 0;
        margin-bottom: -15%;
        position: absolute;
    }
`;

export const StatContainerLast = styled(StatContainer)`
    left: auto;
    right: -10%;
    transform: none;

    img {
        max-height: 210px;
    }
`;

export const PaddedBox = styled.div`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding: ${responsiveSpacing(spacing.m)};
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        padding: ${responsiveSpacing(spacing.l)};
    }
`;
