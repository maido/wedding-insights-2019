/**
 * @prettier
 * @flow
 */
import React from 'react';
import Image from 'react-image';
import {Parallax} from 'react-scroll-parallax';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import ContentHero from '../ContentHero';
import FeaturedInstagramAccount from '../FeaturedInstagramAccount';
import ProductStat from '../ProductStat';
import PreloadedImage from '../PreloadedImage';
import {themes} from '../../globals/variables';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    hasBeenSeen: boolean,
    isVisible: boolean,
    section: *
};

const ContentSectionFashion = ({hasBeenSeen, isVisible, section}: Props) => {
    const theme = themes[section.theme];

    return (
        <S.Wrapper theme={section.theme}>
            <ContentHero
                subtitle={section.title}
                title={section.shortTitle}
                text={section.text}
                theme={section.theme}
            />

            <Container>
                <UI.LayoutContainer size="grid" stack>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <PreloadedImage
                            image={section.items[0].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />

                        <S.FeaturedInstagramAccountContainer>
                            <Parallax y={[60, -20]}>
                                <FeaturedInstagramAccount
                                    isActive={isVisible}
                                    text={section.featuredInstagrammers.text}
                                    accounts={section.featuredInstagrammers.items}
                                />
                            </Parallax>{' '}
                        </S.FeaturedInstagramAccountContainer>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <AnimateWhenVisible>
                            <S.Label>Fashion #1</S.Label>
                            <S.Heading>{section.items[0].title}</S.Heading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[0].text}} />
                        </AnimateWhenVisible>

                        {hasBeenSeen && (
                            <S.IllustrationDoves>
                                <Image src="/static/images/content/fashion-illustration-1.png" />
                            </S.IllustrationDoves>
                        )}
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.ResponsiveSpacer size="l" sizeAtMobile="grid" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #2</S.Label>
                                <S.Heading>{section.items[1].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[1].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <S.Pattern1 aria-hidden="true" />
                        <PreloadedImage
                            image={section.items[1].images[0]}
                            style={{backgroundColor: theme.accent, zIndex: 2}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.ResponsiveSpacer size="l" sizeAtMobile="none" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <PreloadedImage
                            image={section.items[2].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #3</S.Label>
                                <S.Heading>{section.items[2].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[2].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="xl" sizeAtMobile="grid" />

                <UI.LayoutContainer type="flex-flush" stack>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #4</S.Label>
                                <S.Heading>{section.items[3].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[3].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        {hasBeenSeen && (
                            <S.IllustrationRoseContainer>
                                <Parallax y={[-20, 35]}>
                                    <S.IllustrationRose>
                                        <Image src="/static/images/content/fashion-illustration-2-iiii.png" />
                                    </S.IllustrationRose>
                                </Parallax>
                                <Parallax y={[-20, 35]}>
                                    <S.IllustrationRose>
                                        <Image src="/static/images/content/fashion-illustration-2-iii.png" />
                                    </S.IllustrationRose>
                                </Parallax>
                                <Parallax y={[-20, 35]}>
                                    <S.IllustrationRose>
                                        <Image src="/static/images/content/fashion-illustration-2-ii.png" />
                                    </S.IllustrationRose>
                                </Parallax>
                                <Parallax y={[5, -5]}>
                                    <S.IllustrationRose>
                                        <Image src="/static/images/content/fashion-illustration-2-i.png" />
                                    </S.IllustrationRose>
                                </Parallax>
                            </S.IllustrationRoseContainer>
                        )}
                        <S.StatContainer>
                            <Parallax y={[10, -10]}>
                                <ProductStat
                                    stat={section.items[3].stat.stat}
                                    text={section.items[3].stat.text}
                                    image={section.items[3].stat.image}
                                />
                            </Parallax>
                        </S.StatContainer>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.LayoutContainer size="s">
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <UI.ShowAt breakpoint="mobile">
                            <S.Pattern2 aria-hidden="true" />
                            <PreloadedImage
                                image={section.items[3].images[0]}
                                style={{backgroundColor: theme.accent, zIndex: 2}}
                            />
                        </UI.ShowAt>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <PreloadedImage
                            image={section.items[3].images[1]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="xl" sizeAtMobile="grid" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <PreloadedImage
                            image={section.items[4].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #5</S.Label>
                                <S.Heading>{section.items[4].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[4].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.ResponsiveSpacer size="l" sizeAtMobile="none" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #6</S.Label>
                                <S.Heading>{section.items[5].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[5].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <S.Pattern3 aria-hidden="true" />
                        <PreloadedImage
                            image={section.items[5].images[0]}
                            style={{backgroundColor: theme.accent, zIndex: 2}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="l" sizeAtMobile="grid" />

                <UI.LayoutContainer type="flex-flush" stack>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #7</S.Label>
                                <S.Heading>{section.items[6].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[6].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        {hasBeenSeen && (
                            <S.IllustrationFlowers>
                                <Image src="/static/images/content/fashion-illustration-3.png" />
                            </S.IllustrationFlowers>
                        )}
                        <S.StatContainerHorizontal>
                            <Parallax y={[20, -20]}>
                                <ProductStat
                                    layout="horizontal"
                                    stat={section.items[6].stat.stat}
                                    text={section.items[6].stat.text}
                                />
                            </Parallax>
                        </S.StatContainerHorizontal>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.ResponsiveSpacer size="m" sizeAtMobile="none" />

                <UI.LayoutContainer size="s">
                    <UI.LayoutItem sizeAtMobile={1 / 1} data-wide>
                        <PreloadedImage
                            image={section.items[6].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="s" sizeAtMobile="grid" />
                <UI.Spacer size="none" sizeAtMobile="l" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <S.Pattern2 aria-hidden="true" />
                        <PreloadedImage
                            image={section.items[7].images[0]}
                            style={{backgroundColor: theme.accent, zIndex: 2}}
                        />
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #8</S.Label>
                                <S.Heading>{section.items[7].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[7].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.ResponsiveSpacer size="l" sizeAtMobile="none" />

                <UI.LayoutContainer type="flex-flush" data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.PaddedBox>
                                <S.Label>Fashion #9</S.Label>
                                <S.Heading>{section.items[8].title}</S.Heading>
                                <S.Text dangerouslySetInnerHTML={{__html: section.items[8].text}} />
                            </S.PaddedBox>
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <PreloadedImage
                            image={section.items[8].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="xl" sizeAtMobile="grid" />

                <UI.LayoutContainer size="grid" stack>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <PreloadedImage
                            image={section.items[9].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                        <S.StatContainerLast>
                            <Parallax y={[60, -20]}>
                                <ProductStat
                                    stat={section.items[9].stat.stat}
                                    text={section.items[9].stat.text}
                                    image={section.items[9].stat.image}
                                />
                            </Parallax>
                        </S.StatContainerLast>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12}>
                        <AnimateWhenVisible>
                            <S.Label>Fashion #10</S.Label>
                            <S.Heading>{section.items[9].title}</S.Heading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[9].text}} />
                        </AnimateWhenVisible>

                        <UI.Spacer size="grid" />

                        {hasBeenSeen && (
                            <S.IllustrationWreath>
                                <Image
                                    src="/static/images/content/fashion-illustration-4.png"
                                    aria-hidden="true"
                                />
                            </S.IllustrationWreath>
                        )}
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </S.Wrapper>
    );
};

export default ContentSectionFashion;
