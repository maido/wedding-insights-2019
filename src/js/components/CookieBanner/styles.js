/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Wrapper = styled.section`
    background-color: ${themes.primary.background};
    bottom: 0;
    color: ${themes.primary.text};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(12)};
    line-height: 1.4;
    padding: ${rem(spacing.s)};
    position: fixed;
    transform: translateY(${rem(200)});
    transition: ${transitions.slow};
    will-change: transform;
    width: 100%;
    z-index: 200;

    ${props => (props.isVisible ? 'transform: translateY(0);' : '')};

    a {
        color: ${colors.white};
    }
    a:hover,
    a:focus {
        color: ${colors.red};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: flex;
        font-size: ${rem(14)};
        justify-content: center;
    }
`;

export const CTAContainer = styled.div`
    align-items: center;
    display: flex;
    flex: 0 0 ${rem(200)};
    justify-content: center;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-top: ${rem(spacing.s)};
    }
`;

export const CloseButton = styled.button`
    background: none;
    border: 0;
    cursor: pointer;
    margin-left: ${rem(spacing.m)};
    outline: none;
    padding: 0;
`;

export const SVG = styled.svg`
    height: ${rem(spacing.s)};
    width: ${rem(spacing.s)};

    * {
        fill: ${colors.white};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        height: ${rem(spacing.m)};
        width: ${rem(spacing.m)};
    }
`;
