/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {breakpoints} from '../../globals/variables';

export const Container = styled.div`
    height: 0;
    padding-bottom: 122.4%;
    position: relative;
    perspective: 1000px;
    transform-style: preserve-3d;
    width: 100%;

    div {
        height: 100%;
        position: absolute;
        transform-origin: center center;
        will-change: transform;
        width: 100%;
    }

    img {
        will-change: transform;
    }
`;

export const Frame = styled.img`
    bottom: 0;
    left: 0;
    position: absolute;
    right: 0;
    top: 0;
    transform-origin: center center;
    width: 100%;
    z-index: 4;
`;

export const Pattern = styled.img`
    left: 0;
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-70%);
    transform-origin: center center;
    width: 100%;
    z-index: 1;
`;

export const Photo = styled.img`
    left: 50%;
    height: 82%;
    position: absolute;
    right: 0;
    top: 11%;
    transform: translateX(-50%);
    transform-origin: center center;
    width: auto;
    z-index: 1;
`;

export const Like = styled.img`
    left: 49%;
    position: absolute;
    top: 6%;
    transform-origin: center center;
    width: ${between('70px', '80px')};
    z-index: 5;

    ${props => props.photo === 2 && `left: 26%; top: 7%`}
    ${props => props.photo === 3 && `left: 41%; top: -8%`}

    @media (min-width: ${rem(breakpoints.mobile)}) {
        left: 49%;
        top: 8%;

        ${props => props.photo === 2 && `left: 28%; top: 8%`}
        ${props => props.photo === 3 && `left: 42%; top: 4%`}
    }
`;
