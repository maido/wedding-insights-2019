/**
 * @prettier
 * @flow
 */
import React, {useEffect, useState} from 'react';
import {useTransition, animated} from 'react-spring/web.cjs';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {isActive: boolean};

const InstagramHero = ({isActive}: Props) => {
    const photos = [1, 2, 3];
    const [index, setIndex] = useState(0);

    const photoProps = useTransition(photos[index], i => i, {
        config: {mass: 2, tension: 150, friction: 30},
        from: {
            opacity: 0,
            position: 'absolute',
            transform: 'scale(0.9) rotateX(50deg)',
            zIndex: 2
        },
        enter: {opacity: 1, transform: 'scale(1) rotateX(0)', zIndex: 2},
        leave: {opacity: 0, transform: 'scale(0.9) rotateX(50deg)', zIndex: 2}
    });
    const likeProps = useTransition(photos[index], i => i, {
        config: {mass: 5},
        from: {opacity: 0, transform: 'rotate(-2deg) translateY(50px)', zIndex: 4},
        enter: {opacity: 1, transform: 'rotate(0) translateY(0)', zIndex: 4},
        leave: {opacity: 0, transform: ''}
    });
    const patternProps = useTransition(0, null, {
        config: {mass: 1, tension: 150, friction: 30},
        from: {opacity: 0, transform: 'scale(0)'},
        enter: {opacity: 1, transform: 'scale(1)'},
        leave: {opacity: 0}
    });
    const frameProps = useTransition(0, null, {
        config: {mass: 1, tension: 150, friction: 30},
        from: {
            opacity: 0,
            transform: 'scale(1.4) rotate(-20deg) translate(20px, 20px)',
            zIndex: 3
        },
        enter: {opacity: 1, transform: 'scale(1) rotate(0) translate(0,0)', zIndex: 3},
        leave: {opacity: 0, transform: ''}
    });

    useEffect(() => {
        let interval;

        if (isActive) {
            interval = setInterval(
                () => setIndex(prevIndex => (prevIndex + 1) % photos.length),
                3000
            );
        }

        return () => {
            if (isActive) {
                clearInterval(interval);
            }
        };
    }, [isActive]);

    return (
        <S.Container>
            {patternProps.map(({item, key, props}) => (
                <animated.div key={key} style={props}>
                    <S.Pattern src="/static/images/line-pattern-2.png" aria-hidden="true" />
                </animated.div>
            ))}

            {photoProps.map(({item, key, props}) => (
                <div key={key}>
                    <animated.div style={props}>
                        <S.Photo
                            src={`/static/images/hero-instagram-photo-${item}.png`}
                            srcSet={`/static/images/hero-instagram-photo-${item}.png 1x, /static/images/hero-instagram-photo-${item}@2x.png 2x`}
                            aria-hidden="true"
                        />
                    </animated.div>
                </div>
            ))}
            <UI.VisuallyHidden>
                {photos.map(photo => (
                    <S.Photo
                        src={`/static/images/hero-instagram-photo-${photo}.png`}
                        srcSet={`/static/images/hero-instagram-photo-${photo}.png 1x, /static/images/hero-instagram-photo-${photo}@2x.png 2x`}
                        aria-hidden="true"
                        key={photo}
                    />
                ))}
            </UI.VisuallyHidden>

            {frameProps.map(({item, key, props}) => (
                <div key={key}>
                    <animated.div style={props}>
                        <S.Frame src="/static/images/hero-instagram-frame.png" aria-hidden="true" />
                    </animated.div>
                </div>
            ))}

            {likeProps.map(({item, key, props}) => (
                <div key={key}>
                    <animated.div style={props}>
                        <S.Like
                            src={`/static/images/hero-instagram-like-${item}.png`}
                            photo={item}
                            aria-hidden="true"
                        />
                    </animated.div>
                </div>
            ))}
        </S.Container>
    );
};

export default InstagramHero;
