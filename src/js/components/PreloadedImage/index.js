/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import Image from 'react-image';
import VisibilitySensor from 'react-visibility-sensor';
import {useSpring, animated} from 'react-spring/web.cjs';
import {photoURL} from '../../globals/functions';
import * as S from './styles';

type Props = {
    image: {
        filename: string,
        credit?: string,
        alt?: string
    },
    style?: Object
};

export const WIDTHS = [800, 850, 900, 980, 1060, 1600];

const PreloadedImage = ({image, style = {}}: Props) => {
    const [hasBeenVisible, setHasBeenVisible] = useState(false);
    const props = useSpring({
        x: hasBeenVisible ? 100 : 0,
        config: {mass: 1, tension: 210, friction: 20}
    });
    const styles = {
        backgroundColor: '#fff',
        transform: props.x.interpolate(x => `translateX(${x}%`),
        zIndex: 2,
        ...style
    };

    const handleVisibilityChange = (isVisible: boolean) => {
        if (hasBeenVisible === false && isVisible === true) {
            setHasBeenVisible(true);
        }
    };

    const preloadImage = photoURL(`preload_${image.filename}`);
    const hiresImage = photoURL(`${WIDTHS[WIDTHS.length - 2]}_${image.filename}`);
    const srcsetImages = WIDTHS.map(w => `${photoURL(`${w}_${image.filename}`)} ${w}w`).join(',');
    const sizes = `(max-width: ${WIDTHS[WIDTHS.length - 2]}px) 100vw, ${
        WIDTHS[WIDTHS.length - 2]
    }px`;

    return (
        <S.Container style={style}>
            <VisibilitySensor
                onChange={handleVisibilityChange}
                active={hasBeenVisible === false}
                delayedCall={true}
                offset={{top: 100, bottom: 100}}
                partialVisibility={true}
            >
                {({isVisible}) => (
                    <S.ImageContainer isVisible={isVisible}>
                        <Image
                            src={isVisible ? hiresImage : preloadImage}
                            sizes={sizes}
                            srcSet={isVisible ? srcsetImages : ''}
                            loader={<img src={preloadImage} alt="" />}
                            alt={image.alt}
                        />
                        <animated.div style={styles} />
                        {image.credit && (
                            <S.Credit dangerouslySetInnerHTML={{__html: image.credit}} />
                        )}
                    </S.ImageContainer>
                )}
            </VisibilitySensor>
        </S.Container>
    );
};

export default PreloadedImage;
