/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {cover, rem} from 'polished';
import {colors, spacing, transitions} from '../../globals/variables';

export const Container = styled.div`
    overflow: hidden;
    position: relative;
    width: 100%;
`;

export const ImageContainer = styled.div`
    img {
        vertical-align: top;
        width: 100%;
    }

    img + div {
        ${cover()}
        background-color: ${props => props.backgroundColor};
        content: '';
        transform: translateX(0%);
        transition: ${transitions.bezierSlow};
        width: 100%;
    }
`;

export const Credit = styled.div`
    background-color: rgba(0, 0, 0, 0.6);
    border-radius: 2px;
    bottom: ${rem(spacing.xs)};
    color: ${colors.white};
    font-size: ${rem(11)};
    font-weight: 800;
    left: ${rem(spacing.xs)};
    opacitys: 0.8;
    padding: 0 4px;
    position: absolute;
    z-index: 1;

    a {
        color: currentColor;
        text-decoration: underline;
    }
`;
