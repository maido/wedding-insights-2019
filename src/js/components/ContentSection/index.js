/**
 * @prettier
 * @/flow
 */
import React from 'react';
import Image from 'react-image';
import Container from '../Container';
import ContentHero from '../ContentHero';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {section: *};

const ContentSection = ({section}: Props) => (
    <S.Wrapper theme={section.theme}>
        <ContentHero title={section.shortTitle} text={section.text} theme={section.theme} />

        <Container>
            {section.items.map(item => (
                <div key={item.title}>
                    <UI.Heading3>{item.title}</UI.Heading3>
                    {item.images
                        .filter(image => image.hires.length)
                        .map(image => (
                            <Image
                                key={image.hires}
                                src={image.hires}
                                loader={
                                    <div>
                                        <img src={image.lowres} />
                                    </div>
                                }
                            />
                        ))}
                    <div style={{height: 400}} />
                </div>
            ))}
        </Container>
    </S.Wrapper>
);

export default ContentSection;
