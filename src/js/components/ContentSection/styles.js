/**
 * @prettier
 */
import styled from '@emotion/styled';
import {themes} from '../../globals/variables';

export const Wrapper = styled.section`
    ${props =>
        props.theme &&
        `
        background-color: ${themes[props.theme].background};
        color: ${themes[props.theme].text}`}
`;
