/**
 * @prettier
 * @flow
 */
import React from 'react';
import {withTheme} from 'emotion-theming';
import * as S from './styles';

type Props = {
    handleLinkClick: Function,
    isVisible: boolean,
    navigation: Array<{
        url: string,
        label: string,
        isActive: boolean
    }>,
    theme: Object,
    visibleSection: string
};

const FixedSiteNav = ({
    handleLinkClick,
    isVisible,
    navigation = [],
    theme,
    visibleSection
}: Props) => (
    <S.Container style={{opacity: isVisible ? 1 : 0}}>
        <S.Nav>
            {navigation.map(link => (
                <S.Link
                    href={link.url}
                    key={link.label}
                    theme={theme}
                    isActive={link.label === visibleSection}
                    onClick={event => {
                        handleLinkClick(event, link.url);
                    }}
                >
                    {link.label}
                </S.Link>
            ))}
        </S.Nav>
    </S.Container>
);

export default withTheme(FixedSiteNav);
