/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing, transitions} from '../../globals/variables';
import {CONTAINER_WIDTH} from '../Container/styles';

export const Container = styled.div`
    font-size: ${rem(16)};
    font-weight: 600;
    letter-spacing: 1px;
    opacity: 0;
    position: relative;
    transition: opacity 0.4s ease-out;
    z-index: 100;

    @media (min-width: ${rem(CONTAINER_WIDTH + 50)}) {
        font-size: ${rem(18)};
    }
`;

export const Nav = styled.nav`
    right: ${rem(spacing.s)};
    position: fixed;
    transform: rotate(90deg) translateX(50%);
    transform-origin: right;
    top: 50vh;
    z-index: 201;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        right: ${rem(spacing.s)};
    }

    @media (min-width: ${rem(CONTAINER_WIDTH + 50)}) {
        right: ${rem(spacing.l)};
    }
`;

export const Link = styled.a`
    color: ${props => props.theme.link};
    margin-right: ${rem(spacing.m)};
    position: relative;

    &::after {
        background-color: ${props => props.theme.accent};
        bottom: ${rem(-2)};
        content: '';
        height: 2px;
        left: 0;
        position: absolute;
        transition: ${transitions.default};
        width: 0;
    }

    &:hover,
    &:focus {
        color: ${props => props.theme.link};
    }
    &:hover::after,
    &:focus::after {
        width: 100%;
    }

    ${props => props.isActive && `&::after {width: 100%;}`}

    &:last-of-type {
        margin-right: 0;
    }
`;
