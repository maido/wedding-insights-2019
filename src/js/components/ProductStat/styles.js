/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {breakpoints, fontFamilies, spacing, themes} from '../../globals/variables';

export const Text = styled.span`
    color: ${themes.light.text};
    display: block;
`;

export const Stat = styled.span`
    display: flex;
    color: ${themes.light.accent};
    font-size: ${between('60px', '75px')};
    font-family: ${fontFamilies.bold};
    font-weight: 800;
    line-height: 1;
    justify-content: center;
    margin-bottom: ${rem(spacing.s)};
    // min-width: 20%;

    span:first-of-type {
        align-self: flex-start;
        font-size: ${between('40px', '50px')};
    }
    span:last-of-type {
        align-self: flex-start;
        font-size: ${between('15px', '22px')};
        margin-left: ${rem(2)};
        margin-top: ${rem(12)};
    }
`;

export const Container = styled.div`
    background-color: ${themes.light.background};
    font-family: ${fontFamilies.bold};
    padding: ${rem(38)} ${rem(30)};
    text-align: center;
    position: relative;
    z-index: 10;

    ${props =>
        props.layout === 'horizontal' &&
        `
        align-items: center;
    display: flex;
    text-align: left;

    ${Stat} {
        font-size: ${between('50px', '60px')};
        margin-right: ${rem(spacing.m)};
        margin-bottom: 0;
    }`}

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;

export const ImageContainer = styled.div`
    margin-bottom: ${rem(spacing.m * -1)};
    min-height: 150px;
    max-height: 200px;
    padding: 0 ${rem(spacing.m)};

    img {
        // height: 100%;
    }
`;
