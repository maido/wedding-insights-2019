/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import Image from 'react-image';
import AnimatedNumber from '../AnimatedNumber';
import {photoURL} from '../../globals/functions';
import * as S from './styles';

type Props = {
    layout?: string,
    image?: string,
    stat: string,
    text: string
};

const ProductStat = ({layout = 'vertical', image, stat, text}: Props) => (
    <S.Container layout={layout}>
        {image && (
            <S.ImageContainer>
                <Image src={photoURL(image)} alt="Product shot relating to the stat" />
            </S.ImageContainer>
        )}
        {stat.includes('%') && (
            <S.Stat>
                <span>+</span>
                <AnimatedNumber num={stat} />
                <span>%</span>
            </S.Stat>
        )}
        {!stat.includes('%') && <S.Stat>{stat}</S.Stat>}
        <S.Text>{text}</S.Text>
    </S.Container>
);

export default ProductStat;
