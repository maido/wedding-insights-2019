/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {useSpring, animated} from 'react-spring/web.cjs';
import Container from '../Container';
import DelayRender from '../DelayRender';
import InstagramHero from '../InstagramHero';
import LystLogo from '../LystLogo';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    handleLinkClick: Function,
    isVisible: boolean,
    navigation: Array<{
        url: string,
        label: string,
        isActive: boolean
    }>,
    subtitle: string,
    text: string,
    title: string
};

const SiteHeader = ({
    handleLinkClick,
    isVisible,
    navigation = [],
    subtitle,
    text,
    title
}: Props) => {
    const titleProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        from: {opacity: 0, transform: 'scale(1.05) translateY(50px) rotateX(50deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    });
    const subtitleProps = useSpring({
        config: {mass: 1, tension: 150, friction: 30},
        from: {opacity: 0, transform: 'translateY(25px)'},
        to: {opacity: 1, transform: 'translateY(0)'}
    });

    return (
        <S.Container>
            <S.DividerContainer />
            <S.Texture />
            <Container>
                <UI.HideAt breakpoint="mobile">
                    <animated.div style={titleProps}>
                        <S.LogoContainer>
                            <LystLogo />
                        </S.LogoContainer>
                        <S.Title>{title}</S.Title>
                    </animated.div>
                </UI.HideAt>

                <UI.LayoutContainer size="xl" data-text>
                    <UI.LayoutItem sizeAtMobile={6 / 12} sizeAtTablet={5 / 12}>
                        <S.TextContainer>
                            <UI.ShowAt breakpoint="mobile">
                                <animated.div style={titleProps}>
                                    <LystLogo />
                                    <S.Title>{title}</S.Title>
                                </animated.div>
                            </UI.ShowAt>

                            <animated.div style={subtitleProps}>
                                <S.Subtitle> {subtitle}</S.Subtitle>

                                <p dangerouslySetInnerHTML={{__html: text}} />

                                <nav>
                                    {navigation.map(link => (
                                        <S.Link
                                            href={link.url}
                                            key={link.label}
                                            isActive={link.isActive}
                                            onClick={e => {
                                                handleLinkClick(e, link.url);
                                            }}
                                        >
                                            {link.label}
                                        </S.Link>
                                    ))}
                                </nav>
                            </animated.div>
                        </S.TextContainer>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} sizeAtTablet={7 / 12}>
                        <S.ImageContainer>
                            <DelayRender delay={750}>
                                <InstagramHero isActive={isVisible} />
                            </DelayRender>
                        </S.ImageContainer>
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </S.Container>
    );
};

export default SiteHeader;
