/**
 * @prettier
 * @/flow
 */
import styled from '@emotion/styled';
import {between, cover, rem} from 'polished';
import {breakpoints, colors, fontFamilies, spacing, transitions} from '../../globals/variables';

export const Container = styled.header`
    background-color: ${colors.tertiary};
    color: ${colors.primary};
    padding-bottom: ${rem(140)};
    padding-bottom: 7.5vmax;
    position: relative;
    z-index: 10;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        [data-text] {
            display: flex;
            flex-direction: column;
        }

        [data-text] > div:nth-of-type(1) {
            order: 2;
            padding-top: ${rem(spacing.m)};
        }
        [data-text] > div:nth-of-type(2) {
            order: 1;
        }
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        [data-text] {
            align-items: center;
            display: flex;
            margin-top: ${rem(spacing.m * -1)};
        }
    }
`;

export const LogoContainer = styled.div`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        text-align: center;
    }
`;

export const DividerContainer = styled.div`
    background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTQ0MiIgaGVpZ2h0PSIzODAiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZD0iTS44ODY4NiAzNzMuOTk2M2wzMjguNTE3NTItNTYuMTU2IDI0Ni42NTI0NC03Mi4xODExIDE4Ny45NTkyLTI5LjY4ODQgMTk3LjQ0NDAyLTY0LjkxNjg1IDE3My45NDcxNy00MS42NDI4MSAxNDkuMTc3MTQtMzMuODQ1NTMgMTA0LjQzMDAyLTU4LjE0MjgxTDE0NDEuNTAxOTUuMTA4djM3OS4wNjExMUguODg2ODZ6IiBmaWxsPSIjMDczOTJEIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiLz48L3N2Zz4=),
        url('/static/images/noise-texture.png');
    background-size: 100% auto, 100px 100px;
    background-position: bottom center;
    background-repeat: no-repeat;
    ${cover()}
    bottom: -20px;
    left: -2%;
    width: 104%;
`;

export const Texture = styled.div`
    background-image: url('/static/images/noise-texture.png');
    background-size: 100px 100px;
    ${cover()}
`;

export const Title = styled.h1`
    display: block;
    font-family: ${fontFamilies.heading};
    font-size: ${rem(140)};
    font-weight: 300;
    letter-spacing: ${rem(-1)};
    line-height: 1;
    margin-bottom: 0;
    margin-top: ${rem(spacing.s)};
    text-transform: uppercase;

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(230)};
        margin-left: ${between('-10px', '-30px')};
        margin-top: ${rem(spacing.m)};
    }

    @media (min-width: ${rem(1300)}) {
        margin-left: ${between('-20px', '-60px')};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        text-align: center;
    }
`;

export const Subtitle = styled.span`
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${rem(24)};
    line-height: 1.3;
    margin-bottom: ${rem(spacing.m)};
    margin-top: ${rem(spacing.m * -1)};

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(30)};
    }
`;

export const TextContainer = styled.div`
    text-align: center;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        text-align: left;
    }

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        font-size: ${rem(18)};
    }
`;

export const Link = styled.a`
    color: ${colors.wewak};
    font-family: ${fontFamilies.bold};
    margin-right: ${rem(spacing.m)};
    position: relative;

    &::after {
        background-color: ${colors.wewak};
        bottom: ${rem(-2)};
        content: '';
        height: 2px;
        left: 0;
        position: absolute;
        transition: ${transitions.default};
        width: 100%;
    }
    &:hover::after,
    &:active::after {
        background-color: ${colors.primary};
    }

    &:last-of-type {
        margin-right: 0;
    }
`;

export const ImageContainer = styled.div`
    position: relative;
    width: 100%;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        margin-left: -7.5%;
        width: 115%;
    }

    @media (max-width: ${rem(breakpoints.tabletSmall)}) and (min-width: ${rem(
            breakpoints.mobile
        )}) {
        margin-left: -7.5%;
        width: 120%;
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        padding-top: ${rem(spacing.m)};
    }
`;
