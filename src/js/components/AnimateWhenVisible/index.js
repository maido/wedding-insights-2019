/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import {useSpring, animated} from 'react-spring/web.cjs';
import {spacing} from '../../globals/variables';

type Props = {
    children: React.Node,
    delay?: number,
    config?: Object,
    offset?: Object
};

const AnimateWhenVisible = ({children, delay = 0, config, offset = {}}: Props) => {
    const defaultConfig = {
        config: {},
        from: {transform: `translateY(${spacing.l}px)`, opacity: 0},
        to: {transform: `translateY(0)`, opacity: 1}
    };
    const springConfig = {
        ...defaultConfig,
        ...config
    };

    const [styleProps, setStyleProps] = useSpring(() => ({
        config: springConfig.config,
        from: springConfig.from
    }));
    const [hasBeenVisible, setHasBeenVisible] = React.useState(false);

    const handleVisibilityChange = (isVisible: boolean) => {
        if (hasBeenVisible === false && isVisible === true) {
            setHasBeenVisible(true);
            setStyleProps(springConfig.to);
        }
    };

    if (delay < 0) {
        handleVisibilityChange(true);
    }

    return (
        <VisibilitySensor
            delayedCall={true}
            onChange={handleVisibilityChange}
            active={hasBeenVisible === false}
            offset={offset}
        >
            <animated.div style={styleProps}>{children}</animated.div>
        </VisibilitySensor>
    );
};

export default AnimateWhenVisible;
