/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {useWindowSize} from '../../globals/hooks';

type Props = {
    children: React.Node,
    handleObservation: Function,
    id: string
};

const isDesktop = size => {
    return size.width && size.width > 800;
};

const ObserveScrolling = ({children, handleObservation, id}: Props) => {
    const windowSize = useWindowSize();
    const ref = React.useRef(null);
    const rootMargin =
        windowSize.height && isDesktop(windowSize) ? `-${windowSize.height / 2 - 100}px` : '-150px';

    React.useEffect(() => {
        const observer = new IntersectionObserver(
            ([entry]) => {
                if (entry.isIntersecting) {
                    handleObservation(id);
                }
            },
            {rootMargin}
        );

        if (ref.current) {
            observer.observe(ref.current);
        }

        return () => {
            if (ref.current) {
                observer.unobserve(ref.current);
            }
        };
    }, []);

    return (
        <div data-element="os" ref={ref}>
            {children}
        </div>
    );
};

export default ObserveScrolling;
