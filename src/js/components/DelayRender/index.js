/**
 * @prettier
 * @flow
 */
import * as React from 'react';

type Props = {
    children: React.Node,
    delay: number
};

const DelayRender = ({children, delay = 0}: Props) => {
    const [hasRendered, setHasRendered] = React.useState(false);

    React.useEffect(() => {
        const timeout = setTimeout(() => setHasRendered(true), delay);

        return () => clearTimeout(timeout);
    }, []);

    return (
        <div key={hasRendered ? 'rendered' : 'notrendered'} style={{opacity: hasRendered ? 1 : 0}}>
            {children}
        </div>
    );
};

export default DelayRender;
