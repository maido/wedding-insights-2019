/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
import {useSpring, animated} from 'react-spring/web.cjs';

type Props = {
    num: string
};

const AnimatedNumber = ({num = ''}: Props) => {
    const [props, set] = useSpring(() => ({n: 0}));
    const [hasBeenVisible, setHasBeenVisible] = React.useState(false);

    const handleVisibilityChange = (isVisible: boolean) => {
        if (hasBeenVisible === false && isVisible === true) {
            let formattedNumber = num.replace('+', '');
            formattedNumber = formattedNumber.replace('%', '');

            setHasBeenVisible(true);
            set({n: parseFloat(formattedNumber)});
        }
    };

    return (
        <VisibilitySensor onChange={handleVisibilityChange} active={hasBeenVisible === false}>
            <animated.div>{props.n.interpolate(n => n.toFixed(0))}</animated.div>
        </VisibilitySensor>
    );
};

export default AnimatedNumber;
