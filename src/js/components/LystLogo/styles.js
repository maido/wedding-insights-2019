/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, transitions} from '../../globals/variables';

export const SVG = styled.svg`
    fill: ${colors.wewak};
    height: ${rem(35)};
    width: auto;

    * {
        transition: ${transitions.default};
    }

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        height: ${rem(48)};
    }
`;
