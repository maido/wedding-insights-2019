/**
 * @prettier
 * @/flow
 */
import React, {useEffect, useRef} from 'react';
import {withTheme} from 'emotion-theming';
import * as UI from '../UI/styles';
import {themes} from '../../globals/variables';

if (typeof window !== 'undefined') {
    const MathUtils = {
        lineEq: (y2, y1, x2, x1, currentVal) => {
            // y = mx + b
            var m = (y2 - y1) / (x2 - x1),
                b = y1 - m * x1;
            return m * currentVal + b;
        },
        lerp: (a, b, n) => (1 - n) * a + n * b
    };

    class Renderer {
        constructor(options, material) {
            this.options = options;
            this.material = material;
            for (let i = 0, len = this.options.uniforms.length; i <= len - 1; ++i) {
                this.material.uniforms[
                    this.options.uniforms[i].uniform
                ].value = this.options.uniforms[i].value;
            }
            for (let i = 0, len = this.options.animatable.length; i <= len - 1; ++i) {
                this[this.options.animatable[i].prop] = this.options.animatable[i].from;
                this.material.uniforms[this.options.animatable[i].prop].value = this[
                    this.options.animatable[i].prop
                ];
            }
            this.currentScroll = window.pageYOffset;
            this.maxScrollSpeed = 400;
            requestAnimationFrame(() => this.render());
        }
        render() {
            const newScroll = window.pageYOffset;
            const scrolled = Math.abs(newScroll - this.currentScroll);
            for (let i = 0, len = this.options.animatable.length; i <= len - 1; ++i) {
                this[this.options.animatable[i].prop] = MathUtils.lerp(
                    this[this.options.animatable[i].prop],
                    Math.min(
                        MathUtils.lineEq(
                            this.options.animatable[i].to,
                            this.options.animatable[i].from,
                            this.maxScrollSpeed,
                            0,
                            scrolled
                        ),
                        this.options.animatable[i].to
                    ),
                    this.options.easeFactor
                );
                this.material.uniforms[this.options.animatable[i].prop].value = this[
                    this.options.animatable[i].prop
                ];
            }
            this.currentScroll = newScroll;
            requestAnimationFrame(() => this.render());
        }
    }

    const materialOptions = {
        type: 'RollingDistortMaterial',
        uniforms: [
            {
                uniform: 'uSineDistortSpread',
                value: 0.44
            },
            {
                uniform: 'uSineDistortCycleCount',
                value: 5
            },
            {
                uniform: 'uSineDistortAmplitude',
                value: 0
            },
            {
                uniform: 'uNoiseDistortVolatility',
                value: 0
            },
            {
                uniform: 'uNoiseDistortAmplitude',
                value: 0.85
            },
            {
                uniform: 'uDistortPosition',
                value: [0, 0]
            },
            {
                uniform: 'uRotation',
                value: 0
            },
            {
                uniform: 'uSpeed',
                value: 0.1
            }
        ],
        animatable: [{prop: 'uSineDistortAmplitude', from: 0, to: 0.1}],
        easeFactor: 0.35
    };

    var material = new Blotter.RollingDistortMaterial();
    var renderer = new Renderer(materialOptions, material);
}

const AnimatedHeading = ({theme, subtitle, title}) => {
    const $container = useRef(null);

    if (typeof window !== 'undefined') {
        var text = new Blotter.Text(title.toUpperCase(), {
            family: 'Chupada Demibold',
            size: 350,
            paddingLeft: 0,
            paddingRight: 0,
            paddingTop: 0,
            paddingBottom: 0,
            fill: themes[theme].heading
        });

        let options = {
            texts: text
        };

        // useEffect(() => {
        //     let blotter = new Blotter(material, options);
        //     let scope = blotter.forText(text);
        //     scope.pause();

        //     const observer = new IntersectionObserver(entries =>
        //         entries.forEach(entry => scope[entry.isIntersecting ? 'play' : 'pause']())
        //     );

        //     if ($container.current) {
        //         observer.observe($container.current);
        //     }
        //     if (scope && $container.current) {
        //         scope.appendTo($container.current);
        //     }
        // }, []);
    }

    return (
        <div ref={$container}>
            <UI.Heading1
                style={{
                    color: themes[theme].heading,
                    fontSize: '20vmax',
                    lineHeight: 1,
                    textTransform: 'uppercase'
                }}
            >
                {title}
            </UI.Heading1>
            {subtitle && (
                <div style={{marginTop: '-4rem', maxWidth: 650}}>
                    <UI.Heading2 as="span" style={{color: 'white'}}>
                        {subtitle}
                    </UI.Heading2>
                </div>
            )}
        </div>
    );
};

export default withTheme(AnimatedHeading);
