/**
 * @prettier
 * @flow
 */
import React from 'react';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import * as S from './styles';

type Props = {
    subtitle?: string,
    title: string,
    text: string,
    theme: string
};

const ContentHero = ({subtitle, text, theme, title}: Props) => {
    const headingProps = {
        config: {mass: 1, tension: 80, friction: 35},
        from: {opacity: 0, transform: 'scale(1.05) translateY(50px) rotateX(50deg)'},
        to: {opacity: 1, transform: 'scale(1) translateY(0) rotateX(0)'}
    };
    const subheadingProps = {
        from: {opacity: 0, transform: 'translateY(50px)'},
        to: {opacity: 1, transform: 'translateY(0)'}
    };

    return (
        <S.Wrapper>
            <Container verticalPadding={false}>
                <AnimateWhenVisible config={headingProps} offset={{bottom: -150}}>
                    <S.Heading theme={theme}>{title}</S.Heading>
                </AnimateWhenVisible>
                {subtitle && (
                    <AnimateWhenVisible config={subheadingProps}>
                        <S.Subheading>{subtitle}</S.Subheading>
                    </AnimateWhenVisible>
                )}

                <S.Intro dangerouslySetInnerHTML={{__html: text}} />
            </Container>
        </S.Wrapper>
    );
};

export default ContentHero;
