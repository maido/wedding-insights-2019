/**
 * @prettier
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes
} from '../../globals/variables';

export const Wrapper = styled.section`
    margin-bottom: ${responsiveSpacing(spacing.m)};
    position: relative;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-bottom: ${responsiveSpacing(100)};
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        text-align: center;
    }
`;

export const Heading = styled.h2`
    ${props => props.theme && `color: ${themes[props.theme].heading};`}
    font-family: ${fontFamilies.heading};
    font-size: ${between('120px', '260px')};
    font-weight: normal;
    line-height: .825;
    margin: 0;
    text-transform: uppercase;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${between('180px', '300px')};
    }
`;

export const Subheading = styled.span`
    display: block;
    font-family: ${fontFamilies.bold};
    font-size: ${between('24px', '32px')};
    font-weight: normal;
    line-height: 1.2;
    margin: ${rem(spacing.xs)};
    text-transform: capitalize;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${between('30px', '34px')};
        max-width: 80%;
    }
`;

export const Intro = styled.p`
    color: ${colors.black};
    margin-bottom: 0;
    max-width: ${rem(650)};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.lead)};
    }
`;
