/**
 * @prettier
 */
import {css, keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';

const fadeInUpAnimation = keyframes`
    from {
        opacity: 0;
        transform: translateY(${rem(spacing.s)});
    } 
    to {
        opacity: 1;
        transform: translateY(0);
    }
`;

export const FadeInUp = styled.div`
    animation: ${fadeInUpAnimation} 0.6s;
    animation-delay: ${props => (props.delay ? `${props.delay / 1000}s` : 0)};
    animation-fill-mode: forwards;
    opacity: 0;
    position: relative;
    transform: translateY(${rem(spacing.m)});
    transition: ${transitions.bezier};
`;

//

const Heading = css`
    font-family: ${fontFamilies.heading};
    font-weight: 300;
    letter-spacing: ${rem(-1)};
    line-height: 1;
    margin-bottom: ${rem(spacing.m)};
    margin-top: 0;
    text-transform: uppercase;
`;

export const Heading1 = styled.h1`
    ${Heading};
    font-size: ${rem(fontSizes.h1 * 0.75)};

    @media (min-width: ${props => rem(breakpoints.tablet)}) {
        font-size: ${rem(fontSizes.h1)};
    }
`;

export const Heading2 = styled.h2`
    ${Heading};
    font-size: ${rem(fontSizes.h2 * 0.75)};

    @media (min-width: ${props => rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.h2)};
    }
`;

export const Heading3 = styled.h3`
    ${Heading};
    font-size: ${rem(fontSizes.h3)};
`;

export const Heading4 = styled.h4`
    ${Heading};
    font-size: ${rem(fontSizes.h4)};

    ${props => props.primary && `color: ${colors.primary};`}
`;

export const Heading5 = styled.h5`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h5)};
    font-weight: 800;
`;

export const Heading6 = styled.h6`
    ${Heading};
    font-family: ${fontFamilies.bold};
    font-size: ${rem(fontSizes.h6)};
    font-weight: 800;
`;

//

export const Label = styled.span`
    font-size: ${rem(10)};
    font-weight: bold;
    letter-spacing: ${rem(0.1)};
    opacity: 0.9;
    text-transform: uppercase;
`;

//

export const LayoutContainer = styled.div`
    display: block;
    margin: 0;
    padding: 0;
    list-style: none;
    margin-left: ${rem(spacing.m * -1)};
    position: relative;

    ${props =>
        props.size &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
            margin-left: ${rem(spacing[props.size] * -1)};

            > div {
                padding-left: ${rem(spacing[props.size])};
            }
        }
        `};

    ${props =>
        props.stack &&
        `@media (max-width: ${rem(breakpoints.mobile)}) {
            > div + div {
                margin-top: ${rem(spacing.m)};
            }
        }
        `};

    ${props =>
        props.type &&
        props.type === 'flex-flush' &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
            display: flex;
            margin-left: 0;

            > div {
                align-items: center;
                display: flex;
                padding-left: 0;
            }

            img {
                min-height: 100%;
                width: 100%;
            }
        }
        
        @media (max-width: ${rem(breakpoints.mobile)}) {
            [data-text] {
                padding-top: ${rem(spacing.m)};
            }
        }
        `};
`;

const layoutWidth = size => (size * 100).toFixed(2);

export const LayoutItem = styled.div`
    box-sizing: border-box;
    display: inline-block;
    vertical-align: top;
    padding-left: ${rem(spacing.m)};
    position: relative;
    width: ${props => (props.size ? `${layoutWidth(props.size)}%` : '100%')};

    ${props =>
        props.sizeAtMobileSmall &&
        `@media (min-width: ${rem(breakpoints.mobileSmall)}) {
        width: ${layoutWidth(props.sizeAtMobileSmall)}%;
    }`}

    ${props =>
        props.sizeAtMobile &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
        width: ${layoutWidth(props.sizeAtMobile)}%;
    }`}

    ${props =>
        props.sizeAtTablet &&
        `@media (min-width: ${rem(breakpoints.tablet)}) {
        width: ${layoutWidth(props.sizeAtTablet)}%;
    }`}

    ${props =>
        props.sizeAtDesktop &&
        `@media (min-width: ${rem(breakpoints.desktop)}) {
        width: ${layoutWidth(props.sizeAtDesktop)}%;
    }`}


`;

//

export const Lead = styled.p`
    font-size: ${rem(fontSizes.default + 1)};

    @media (min-width: ${rem(breakpoints.tabletSmall)}) {
        font-size: ${rem(fontSizes.lead)};
    }
`;

export const BigLead = styled.p`
    font-size: ${rem(30)};
    font-weight: 600;
`;

//

export const Spacer = styled.div`
    height: ${props => rem(spacing[props.size ? props.size : 'm'])};
    width: ${props => rem(spacing[props.size ? props.size : 'm'])};

    ${props =>
        props.sizeAtMobile &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
        height: ${rem(spacing[props.sizeAtMobile])};
        width: ${rem(spacing[props.sizeAtMobile])};
    }`}
`;

//

export const ResponsiveSpacer = styled.div`
    height: ${props => responsiveSpacing(spacing[props.size ? props.size : 'm'])};
    width: ${props => responsiveSpacing(spacing[props.size ? props.size : 'm'])};

    ${props =>
        props.sizeAtMobile &&
        `@media (min-width: ${rem(breakpoints.mobile)}) {
        height: ${responsiveSpacing(spacing[props.sizeAtMobile])};
        width: ${responsiveSpacing(spacing[props.sizeAtMobile])};
    }`}
`;

export const ResponsivePadding = styled.div`
    padding: ${props => responsiveSpacing(spacing[props.size ? props.size : 'm'])};
`;

//

export const stickyAtMobile = css`
    @media (min-width: ${rem(breakpoints.mobile)}) {
        position: sticky;
        top: ${rem(spacing.m)};
    }
`;

//

export const HideAt = styled.div`
    @media (min-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

export const ShowAt = styled.div`
    @media (max-width: ${props => rem(breakpoints[props.breakpoint])}) {
        display: none;
    }
`;

//

export const VisuallyHidden = styled.span`
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
`;
