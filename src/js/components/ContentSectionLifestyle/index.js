/**
 * @prettier
 * @/flow
 */
import React from 'react';
import {Parallax} from 'react-scroll-parallax';
import AnimateWhenVisible from '../AnimateWhenVisible';
import AnimatedNumber from '../AnimatedNumber';
import Container from '../Container';
import ContentHero from '../ContentHero';
import InstagramIconTag from '../InstagramIconTag';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    isVisible: boolean,
    section: *
};

const ContentSectionLifestyle = ({isVisible = false, section}: Props) => (
    <S.Wrapper>
        <ContentHero
            title={section.shortTitle}
            subtitle={section.title}
            theme={section.theme}
            animate={false}
        />

        <Container>
            <UI.LayoutContainer size="l" stack>
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <UI.Lead dangerouslySetInnerHTML={{__html: section.text}} />
                </UI.LayoutItem>
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <InstagramIconTag tags={section.tags} isActive={isVisible} />
                </UI.LayoutItem>
            </UI.LayoutContainer>

            <UI.ResponsiveSpacer size="xl" sizeAtMobile="grid" />

            <UI.LayoutContainer size="grid">
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <Parallax y={[20, -20]}>
                        <S.StatWrapper>
                            <AnimateWhenVisible>
                                <S.StatStat>
                                    <span>+</span>
                                    <AnimatedNumber num={section.items[0].title} />
                                    <span>%</span>
                                </S.StatStat>
                            </AnimateWhenVisible>
                            <AnimateWhenVisible>
                                <S.Label>Lifestyle #1</S.Label>
                            </AnimateWhenVisible>
                            <AnimateWhenVisible>
                                <S.StatText>{section.items[0].text}</S.StatText>
                            </AnimateWhenVisible>
                        </S.StatWrapper>
                    </Parallax>
                </UI.LayoutItem>
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <AnimateWhenVisible>
                        <S.Label>Lifestyle #2</S.Label>
                        <S.Heading>{section.items[1].title}</S.Heading>
                        <S.Text dangerouslySetInnerHTML={{__html: section.items[1].text}} />
                    </AnimateWhenVisible>

                    <UI.Spacer size="xl" />

                    <AnimateWhenVisible>
                        <S.Label>Lifestyle #3</S.Label>
                        <S.Heading>{section.items[2].title}</S.Heading>
                        <S.Text dangerouslySetInnerHTML={{__html: section.items[2].text}} />
                    </AnimateWhenVisible>
                </UI.LayoutItem>
            </UI.LayoutContainer>

            <UI.ResponsiveSpacer size="xl" sizeAtMobile="grid" />

            <UI.LayoutContainer size="xl">
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <AnimateWhenVisible>
                        <S.Label>Lifestyle #4</S.Label>
                        <S.Heading>{section.items[3].title}</S.Heading>
                        <S.Text dangerouslySetInnerHTML={{__html: section.items[3].text}} />
                    </AnimateWhenVisible>

                    <UI.Spacer size="xl" />

                    <AnimateWhenVisible>
                        <S.Label>Lifestyle #5</S.Label>
                        <S.Heading>{section.items[4].title}</S.Heading>
                        <S.Text dangerouslySetInnerHTML={{__html: section.items[4].text}} />
                    </AnimateWhenVisible>
                </UI.LayoutItem>
                <UI.LayoutItem sizeAtMobile={6 / 12}>
                    <Parallax y={[20, -20]}>
                        <S.StatWrapper>
                            <AnimateWhenVisible>
                                <S.StatStat>{section.items[5].title}</S.StatStat>
                            </AnimateWhenVisible>
                            <AnimateWhenVisible>
                                <S.Label>Lifestyle #6</S.Label>
                            </AnimateWhenVisible>
                            <AnimateWhenVisible>
                                <S.StatText>{section.items[5].text}</S.StatText>
                            </AnimateWhenVisible>
                        </S.StatWrapper>
                    </Parallax>
                </UI.LayoutItem>
            </UI.LayoutContainer>
        </Container>
    </S.Wrapper>
);

export default ContentSectionLifestyle;
