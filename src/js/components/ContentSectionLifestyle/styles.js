/**
 * @prettier
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, fontFamilies, spacing, themes, transitions} from '../../globals/variables';

const theme = themes.primary;

export const Wrapper = styled.section`
    padding: ${responsiveSpacing(75)} 0 ${responsiveSpacing(150)};
    position: relative;
    z-index: 10;

    ${props =>
        props.theme &&
        `color: ${theme.text};
        position: relative;

        &::before {
            background-color: ${theme.background};
            background-image: url('/static/images/noise-texture.png');
            background-size: 100px 100px;
            content: '';
            height: 40%;
            left: 0;
            position: absolute;
            top: 0;
            width: 100%;
            z-index: 0;
        }`}

    @media (max-width: ${rem(breakpoints.mobile)}) {
        padding: ${responsiveSpacing(80)} 0;

        .no-js & {
            background-color: ${theme.background};
            background-image: url('/static/images/noise-texture.png');
            background-size: 100px 100px;
        }
    }
`;

export const StatWrapper = styled.section`
    background-color: rgba(0, 0, 0, 0.15);
    border: 1px solid rgba(255, 255, 255, 0.2);
    border-radius: 2px;
    padding: ${responsiveSpacing(spacing.m)};
    text-align: center;
`;

export const Label = styled.span`
    display: block;
    color: ${theme.accent};
    font-size: ${rem(14)};
    font-weight: 800;
    letter-spacing: ${rem(1)};
    margin-bottom: ${rem(spacing.m)};
    text-transform: uppercase;
`;

export const StatStat = styled.span`
    color: ${theme.accent};
    display: flex;
    font-family: ${fontFamilies.heading};
    font-size: ${between('260px', '330px')};
    line-height: 1;
    margin-bottom: ${rem(spacing.l * -1)};
    padding-bottom: ${responsiveSpacing(spacing.m)};
    padding-top: ${responsiveSpacing(spacing.m)};
    justify-content: center;

    span:first-of-type {
        align-self: center;
        font-size: ${between('100px', '140px')};
        margin-right: ${rem(4)};
    }
    span:last-of-type {
        align-self: flex-start;
        font-size: ${between('50px', '90px')};
        margin-left: ${rem(4)};
    }
`;

export const StatText = styled.p`
    font-size: ${rem(26)};
    font-family: ${fontFamilies.bold};
    line-height: 1.2;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(30)};
    }
`;

export const Heading = styled.h4`
    font-family: ${fontFamilies.heading};
    font-size: ${between('60px', '73px')};
    font-weight: 300;
    line-height: 0.95;
    margin-bottom: ${rem(spacing.s)};
    margin-top: 0;
    text-transform: uppercase;
`;

export const Text = styled.p`
    a {
        color: ${theme.accent};
        font-weight: 600;
        position: relative;
    }
    a::after {
        background-color: ${theme.text};
        bottom: -3px;
        content: '';
        height: 2px;
        left: 0;
        opacity: 0;
        position: absolute;
        transition: ${transitions.default};
        width: 0;
    }

    a:hover,
    a:focus {
        color: ${theme.text};
    }
    a:hover::after,
    a:focus::after {
        opacity: 1;
        width: 100%;
    }
`;
