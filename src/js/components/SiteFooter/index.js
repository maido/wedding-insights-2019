/**
 * @prettier
 * @flow
 */
import React from 'react';
import Image from 'react-image';
import Container from '../Container';
import LystLogo from '../LystLogo';
import SiteSearch from '../SiteSearch';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    search: {
        title: string,
        text: string,
        placeholder: string,
        searchURL: string,
        photo: Photo
    }
};

const SiteFooter = ({search}: Props) => (
    <S.Wrapper>
        <Container>
            <UI.LayoutContainer>
                <UI.LayoutItem sizeAtMobile={6 / 12} sizeAtTablet={5 / 12}>
                    <S.ImageContainer>
                        <Image
                            src="/static/images/footer-illustration.png"
                            loader={<div />}
                            aria-hidden="true"
                        />
                    </S.ImageContainer>
                </UI.LayoutItem>
                <UI.LayoutItem sizeAtMobile={6 / 12} sizeAtTablet={7 / 12}>
                    <LystLogo />

                    <UI.Spacer size="m" />

                    <UI.Heading2 as="h4">{search.title}</UI.Heading2>

                    <SiteSearch
                        label={search.text}
                        placeholder={search.placeholder}
                        url={search.searchURL}
                    />
                </UI.LayoutItem>
            </UI.LayoutContainer>
        </Container>
    </S.Wrapper>
);

export default SiteFooter;
