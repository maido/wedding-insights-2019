/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, spacing, themes} from '../../globals/variables';
import {LayoutContainer} from '../UI/styles';

const theme = themes.primary;

export const Wrapper = styled.section`
    background-color: ${theme.background};
    background-image: url('/static/images/noise-texture.png');
    background-size: 100px 100px;
    color: ${theme.text};
    padding: ${responsiveSpacing(spacing.m)} 0;

    ${LayoutContainer} {
        align-items: center;
        display: flex;
    }

    @media (max-width: ${rem(breakpoints.mobile)}) {
        text-align: center;

        &,
        > * {
            padding-bottom: 0 !important;
        }

        ${LayoutContainer} {
            flex-direction: column;

            > div:nth-of-type(1) {
                order: 2;
            }
            > div:nth-of-type(2) {
                order: 1;
            }
        }
    }
`;

export const ImageContainer = styled.div`
    @media (max-width: ${rem(breakpoints.mobile)}) {
        height: 380px;
        margin-top: ${rem(spacing.m)};
        overflow: hidden;
    }
`;
