/**
 * @prettier
 * @flow
 */
import React from 'react';
import {Parallax} from 'react-scroll-parallax';
import AnimateWhenVisible from '../AnimateWhenVisible';
import Container from '../Container';
import ContentHero from '../ContentHero';
import FeaturedInstagramAccount from '../FeaturedInstagramAccount';
import ProductStat from '../ProductStat';
import PreloadedImage from '../PreloadedImage';
import {themes} from '../../globals/variables';
import * as UI from '../UI/styles';
import * as S from './styles';

type Props = {
    hasBeenSeen: boolean,
    isVisible: boolean,
    section: *
};

const ContentSectionTrendsetters = ({hasBeenSeen, section}: Props) => {
    const theme = themes[section.theme];

    return (
        <S.Wrapper theme={section.theme}>
            <ContentHero
                title="Trend setters"
                subtitle={section.title}
                text={section.text}
                theme={section.theme}
            />

            <Container>
                <UI.LayoutContainer size="grid" stack data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.Label>Trendsetters #1</S.Label>
                            <S.Heading>{section.items[0].title}</S.Heading>
                            <S.Subheading>{section.items[0].subtitle}</S.Subheading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[0].text}} />
                        </AnimateWhenVisible>
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <S.IllustrationRings
                            src="/static/images/content/trendsetters-illustration-1.png"
                            aria-hidden="true"
                        />

                        <S.StatContainer1>
                            <Parallax y={[20, -20]}>
                                <ProductStat
                                    layout="horizontal"
                                    stat={section.items[0].stat.stat}
                                    label={section.items[0].stat.label}
                                    text={section.items[0].stat.text}
                                />
                            </Parallax>
                        </S.StatContainer1>
                    </UI.LayoutItem>
                    <UI.LayoutItem>
                        <UI.LayoutContainer size="s" stackdata-text-image>
                            <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                                <S.Pattern1 alt="" />
                                <PreloadedImage
                                    image={section.items[0].images[0]}
                                    style={{backgroundColor: theme.accent, zIndex: 2}}
                                />
                            </UI.LayoutItem>
                            <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                                <UI.ShowAt breakpoint="mobile">
                                    <PreloadedImage
                                        image={section.items[0].images[1]}
                                        style={{backgroundColor: theme.accent}}
                                    />
                                </UI.ShowAt>
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="xl" sizeAtMobile="grid" />

                <UI.LayoutContainer size="grid" stack data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <PreloadedImage
                            image={section.items[1].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.Label>Trendsetters #2</S.Label>
                            <S.Heading>{section.items[1].title}</S.Heading>
                            <S.Subheading>{section.items[1].subtitle}</S.Subheading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[1].text}} />
                        </AnimateWhenVisible>

                        {hasBeenSeen && (
                            <UI.ShowAt breakpoint="mobile">
                                <S.IllustrationPearls
                                    src="/static/images/content/trendsetters-illustration-2.png"
                                    aria-hidden="true"
                                />
                            </UI.ShowAt>
                        )}

                        <S.StatContainer2>
                            <Parallax y={[20, -20]}>
                                <ProductStat
                                    stat={section.items[1].stat.stat}
                                    image={section.items[1].stat.image}
                                    text={section.items[1].stat.text}
                                />
                            </Parallax>
                        </S.StatContainer2>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="l" sizeAtMobile="none" />

                <UI.LayoutContainer size="grid" stack data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.Label>Trendsetters #3</S.Label>
                            <S.Heading>{section.items[2].title}</S.Heading>
                            <S.Subheading>{section.items[2].subtitle}</S.Subheading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[2].text}} />
                        </AnimateWhenVisible>
                        <UI.ShowAt breakpoint="mobile">
                            <UI.Spacer size="l" />
                        </UI.ShowAt>
                    </UI.LayoutItem>
                    <UI.LayoutItem data-image>
                        <UI.LayoutContainer size="s">
                            <UI.LayoutItem sizeAtMobile={6 / 12}>
                                <PreloadedImage
                                    image={section.items[2].images[0]}
                                    style={{backgroundColor: theme.accent}}
                                />
                            </UI.LayoutItem>
                            <UI.LayoutItem sizeAtMobile={6 / 12}>
                                <UI.ShowAt breakpoint="mobile">
                                    <S.StatContainer4>
                                        <Parallax y={[20, -20]}>
                                            <ProductStat
                                                layout="horizontal"
                                                stat={section.items[2].stat.stat}
                                                text={section.items[2].stat.text}
                                            />
                                        </Parallax>
                                    </S.StatContainer4>

                                    <PreloadedImage
                                        image={section.items[2].images[1]}
                                        style={{backgroundColor: theme.accent}}
                                    />
                                </UI.ShowAt>
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="l" sizeAtMobile="grid" />
                <UI.Spacer size="none" sizeAtMobile="xl" />

                <UI.LayoutContainer size="grid" stack data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} />
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <UI.Spacer size="m" sizeAtMobile="none" />
                        <AnimateWhenVisible>
                            <S.Label>Trendsetters #4</S.Label>
                            <S.Heading>{section.items[3].title}</S.Heading>
                            <S.Subheading>{section.items[3].subtitle}</S.Subheading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[3].text}} />
                        </AnimateWhenVisible>
                        <UI.Spacer size="l" />
                    </UI.LayoutItem>
                    <UI.LayoutItem data-image>
                        <UI.LayoutContainer>
                            <UI.LayoutItem data-wide>
                                <S.StatContainer3>
                                    <Parallax y={[20, -20]}>
                                        {hasBeenSeen && (
                                            <S.IllustrationCrown
                                                src="/static/images/content/trendsetters-illustration-3.png"
                                                aria-hidden="true"
                                            />
                                        )}
                                        <ProductStat
                                            stat={section.items[3].stat.stat}
                                            image={section.items[3].stat.image}
                                            text={section.items[3].stat.text}
                                        />
                                    </Parallax>
                                </S.StatContainer3>
                                <S.Pattern2 alt="" />
                                <PreloadedImage
                                    image={section.items[3].images[0]}
                                    style={{backgroundColor: theme.accent, zIndex: 2}}
                                />
                            </UI.LayoutItem>
                        </UI.LayoutContainer>
                    </UI.LayoutItem>
                </UI.LayoutContainer>

                <UI.Spacer size="s" sizeAtMobile="grid" />
                <UI.Spacer size="none" sizeAtMobile="xl" />

                <UI.LayoutContainer size="grid" stack data-text-image>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-text>
                        <AnimateWhenVisible>
                            <S.Label>Trendsetters #5</S.Label>
                            <S.Heading>{section.items[4].title}</S.Heading>
                            <S.Subheading>{section.items[4].subtitle}</S.Subheading>
                            <S.Text dangerouslySetInnerHTML={{__html: section.items[4].text}} />
                        </AnimateWhenVisible>

                        {hasBeenSeen && (
                            <S.IllustrationDiamonds
                                src="/static/images/content/trendsetters-illustration-4.png"
                                aria-hidden="true"
                            />
                        )}
                    </UI.LayoutItem>
                    <UI.LayoutItem sizeAtMobile={6 / 12} data-image>
                        <S.StatContainerHorizontal>
                            <Parallax y={[20, -20]}>
                                <ProductStat
                                    layout="horizontal"
                                    stat={section.items[4].stat.stat}
                                    text={section.items[4].stat.text}
                                />
                            </Parallax>
                        </S.StatContainerHorizontal>
                        <PreloadedImage
                            image={section.items[4].images[0]}
                            style={{backgroundColor: theme.accent}}
                        />
                        <UI.Spacer />
                    </UI.LayoutItem>
                </UI.LayoutContainer>
            </Container>
        </S.Wrapper>
    );
};

export default ContentSectionTrendsetters;
