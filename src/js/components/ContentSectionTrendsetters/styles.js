/**
 * @prettier
 */
import styled from '@emotion/styled';
import {between, rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, fontFamilies, spacing, themes, transitions} from '../../globals/variables';

const theme = themes.tertiary;

export const Wrapper = styled.section`
    color: ${theme.text};
    padding: ${responsiveSpacing(200)} 0;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        padding: ${responsiveSpacing(80)} 0;

        .no-js & {
            background-color: ${theme.background};
            background-image: url('/static/images/noise-texture.png');
            background-size: 100px 100px;
        }

        [data-text-image] {
            display: flex !important;
            flex-direction: column;

            [data-text] {
                order: 2;
            }
            [data-image] {
                order: 1;
            }
        }
    }
`;

export const FeaturedInstagramAccountContainer = styled.div`
    bottom: ${rem(spacing.l)};
    position: absolute;
    right: ${rem(spacing.grid * 0.85 * -1)};
    max-width: 475px;
    width: calc(100% - ${rem(spacing.grid * 0.85)});
    z-index: 1;
`;

export const Label = styled.span`
    display: block;
    color: ${theme.accent};
    font-size: ${rem(14)};
    font-weight: 800;
    letter-spacing: ${rem(1)};
    margin-bottom: ${rem(spacing.m)};
    text-transform: uppercase;
`;

export const Heading = styled.h4`
    font-family: ${fontFamilies.heading};
    font-size: ${between('60px', '73px')};
    font-weight: 300;
    line-height: 0.95;
    margin: 0;
    text-transform: uppercase;
`;

export const Subheading = styled.span`
    color: ${theme.accent};
    display: block;
    font-family: ${fontFamilies.heading};
    font-size: ${between('18px', '22px')};
    letter-spacing: ${rem(2)};
    margin-bottom: ${rem(spacing.s)};
    text-transform: uppercase;
`;

export const Text = styled.p`
    a {
        color: ${theme.accent};
        font-weight: 800;
        position: relative;
    }
    a::after {
        background-color: ${theme.text};
        bottom: -3px;
        content: '';
        height: 2px;
        left: 0;
        opacity: 0;
        position: absolute;
        transition: ${transitions.default};
        width: 0;
    }

    a:hover,
    a:focus {
        color: ${theme.text};
    }
    a:hover::after,
    a:focus::after {
        opacity: 1;
        width: 100%;
    }
`;

export const IllustrationRings = styled.img`
    margin-top: ${rem(spacing.l * -1)};
    position: relative;
    width: calc(100% + ${rem(75)});
    z-index: 3;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        left: 80%;
        position: absolute;
        top: -100px;
        top: -23vmax;
        width: 50%;
    }
`;

export const IllustrationPearls = styled.img`
    margin-top: ${rem(150)};
    max-width: none;
    width: 120%;
    z-index: 2;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-bottom: ${responsiveSpacing(-100)};
    }
`;

export const IllustrationCrown = styled.img`
    right: -25%;
    position: absolute;
    top: -15%;
    transform: rotate(30deg);
    max-width: 90% !important;
    width: 90%;
    z-index: 11;
`;

export const IllustrationDiamonds = styled.img`
    float: right;
    margin-top: ${rem(80)};
    max-width: 250px;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        width: 40%;
        margin-top: ${rem(spacing.l * -1)};
    }
`;

export const Pattern1 = styled.div`
    background-image: url('/static/images/line-pattern-2.png');
    background-size: auto auto;
    background-repeat: no-repeat;
    bottom: ${rem(spacing.l * -1)};
    height: 0;
    left: ${rem(spacing.l * -1)};
    position: absolute;
    padding-bottom: 40%;
    width: 100%;
    z-index: 1;

    @media (max-width: ${rem(breakpoints.mobile)}) {
        background-size: 400px auto;
        bottom: ${rem(spacing.m * -1)};
    }
`;

export const Pattern2 = styled(Pattern1)`
    background-position: bottom right;
    left: auto;
    right: ${rem(spacing.l * -1)};
`;

export const StatContainer = styled.div`
    left: 50%;
    max-width: 300px;
    position: absolute;
    top: 60%;
    transform: translateX(-50%);
    z-index: 10;
`;

export const StatContainer1 = styled.div`
    margin-left: 10%;
    max-width: 80%;
    position: relative;
    z-index: 10;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-bottom: -15%;
    }
`;

export const StatContainer2 = styled(StatContainer)`
    margin-left: -50%;
    margin-top: -10%;
`;

export const StatContainer3 = styled(StatContainer)`
    left: 25%;
    transform: translateX(-35%);
    top: -45%;

    img {
        max-width: 70%;
    }

    span:first-of-type {
        font-size: ${between('30px', '40px')} !important;
        padding-top: 20px;
    }
`;

export const StatContainer4 = styled.div`
    margin-left: 10%;
    max-width: 80%;
    z-index: 10;

    @media (min-width: ${rem(breakpoints.mobile)}) {
        margin-bottom: -15%;
        position: absolute;
        top: -15%;
    }
`;

export const StatContainerHorizontal = styled.div`
    bottom: ${rem(spacing.l)};
    position: absolute;
    transform: translateX(-20%);
    z-index: 10;
`;

export const StatContainerLast = styled(StatContainer)`
    left: auto;
    max-width: 400px;
    transform: translateX(70%);
`;
