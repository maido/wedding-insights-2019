/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {breakpoints, colors, spacing} from '../../globals/variables';

export const Wrapper = styled.div`
    text-align: center;
    padding: ${rem(spacing.m)};
    position: relative;

    .anim-wrapper {
        top: -2%;
        width: calc(100% - ${rem(spacing.m * 2)});
    }

    @media (max-width: ${rem(breakpoints.tablet)}) {
        margin: 50px 0 -80px;
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        margin-top: 18%;
        padding: 0;

        .anim-wrapper {
            top: -25%;
            width: 100%;
        }
    }
`;

export const Tag = styled.span`
    background-color: ${colors.wewak};
    border-radius: ${rem(6)};
    color: ${colors.white};
    display: inline-block;
    font-size: ${rem(26)};
    font-weight: 800;
    letter-spacing: ${rem(1)};
    line-height: 1;
    padding: 8px 20px 14px 20px;
    position: relative;

    &::after {
        border-style: solid;
        border-width: 15px 15px 0 15px;
        border-color: ${colors.wewak} transparent transparent transparent;
        bottom: -14px;
        content: '';
        height: 0;
        left: 50%;
        position: absolute;
        transform: translateX(-50%);
        width: 0;
    }

    @media (min-width: ${rem(breakpoints.tablet)}) {
        font-size: ${rem(32)};
    }
`;

export const ImageContainer = styled.div`
    margin: ${rem(spacing.l)} auto;
    max-width: ${rem(350)};
`;
