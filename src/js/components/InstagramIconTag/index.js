/**
 * @prettier
 * @flow
 */
import React, {useEffect, useState} from 'react';
import {useTransition, animated} from 'react-spring/web.cjs';
import Image from 'react-image';
import * as S from './styles';

type Props = {
    isActive: boolean,
    tags: Array<string>
};

const InstagramIconTag = ({isActive, tags = []}: Props) => {
    const [index, setIndex] = useState(0);
    const transitions = useTransition(tags[index], i => i, {
        config: {mass: 3, tension: 150, friction: 30},
        from: {
            opacity: 0,
            position: 'absolute',
            transform: 'scale(0.5) translateY(100px)'
        },
        enter: {opacity: 1, transform: 'scale(1) translateY(0)'},
        leave: {opacity: 0, transform: 'scale(0.5) translateY(10px)'}
    });

    useEffect(() => {
        let interval;

        if (isActive) {
            interval = setInterval(
                () => setIndex(prevIndex => (prevIndex + 1) % tags.length),
                2500
            );
        }

        return () => {
            if (isActive) {
                clearInterval(interval);
            }
        };
    }, [isActive]);

    return (
        <S.Wrapper>
            {transitions.map(({item, props, key}) => (
                <animated.div key={key} style={props} className="anim-wrapper">
                    <S.Tag>{item}</S.Tag>
                </animated.div>
            ))}

            <S.ImageContainer>
                <Image src="/static/images/instagram-mask.png" aria-hidden="true" />
            </S.ImageContainer>
        </S.Wrapper>
    );
};

export default InstagramIconTag;
