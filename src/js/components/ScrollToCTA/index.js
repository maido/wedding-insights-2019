/**
 * @prettier
 * @flow
 */
import React from 'react';
import * as S from './styles';

type Props = {
    handleClick: Function,
    isActive: boolean
};

const ScrollToCTA = ({handleClick, isActive = true}: Props) => {
    if (isActive) {
        return (
            <S.Container isActive onClick={handleClick} aria-label="Scroll to the first section">
                <svg
                    fill="none"
                    height="20"
                    stroke="currentColor"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    strokeWidth="2"
                    width="20"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 24 24"
                >
                    <path d="M6 9l6 6 6-6" />
                </svg>
            </S.Container>
        );
    } else {
        return null;
    }
};

export default ScrollToCTA;
