/**
 * @prettier
 */
import {keyframes} from '@emotion/core';
import styled from '@emotion/styled';
import {rem} from 'polished';
import {colors, spacing, transitions} from '../../globals/variables';

const SIZE = 44;

const bounceAnimation = keyframes`
    0%, 100% {
        transform: translateY(0);
    }
    50% {
        transform: translateY(-8px);
    }
`;

export const Container = styled.button`
    animation: ${bounceAnimation} 1s infinite;
    animation-fill-mode: both;
    align-items: center;
    background-color: ${colors.tiber};
    border: 1px solid rgba(255, 255, 255, 0.3);
    box-shadow: 0 10px 80px rgba(0, 0, 0, 0.4);
    border-radius: 100%;
    bottom: ${rem(spacing.l)};
    cursor: pointer;
    color: #fff;
    display: flex;
    height: ${rem(SIZE)};
    left: 50%;
    justify-content: center;
    position: absolute;
    transition: ${transitions.default};
    transform: translateX(-50%);
    width: ${rem(SIZE)};
    z-index: 100;

    &:hover,
    &:active {
        background-color: ${colors.white};
        border-color: ${colors.white};
        color: ${colors.tiber};
    }

    @media (max-height: ${rem(1000)}) {
        bottom: ${rem(spacing.m)};
        position: fixed;
    }
`;
