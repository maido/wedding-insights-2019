/**
 * @prettier
 * @/flow
 */
import * as React from 'react';
import {withTheme} from 'emotion-theming';
import * as S from './styles';

type Props = {
    children: React.Node,
    theme: Object
};

const AnimatedPageBackground = ({children, theme}: Props) => (
    <S.Container theme={theme}>{children}</S.Container>
);

export default withTheme(AnimatedPageBackground);
