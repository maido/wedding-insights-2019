/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';

export const Container = styled.div`
    background-color: ${props => props.theme.background};
    background-image: url('/static/images/noise-texture.png');
    background-size: 100px 100px;
    transition: background 0.3s ease-out;
`;
