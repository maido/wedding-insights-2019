/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import * as S from './styles';

type Props = {
    label: string,
    placeholder: string
};

const SiteSearch = ({label, placeholder}: Props) => {
    const [hasFocus, setHasFocus] = useState(false);

    const handleFocusIn = () => setHasFocus(true);
    const handleFocusOut = () => setHasFocus(false);

    return (
        <form action="https://www.lyst.com/search" method="get" target="_blank" validate="false">
            <S.Label htmlFor="q">{label}</S.Label>
            <S.Wrapper hasFocus={hasFocus}>
                <S.Input
                    type="text"
                    name="q"
                    id="q"
                    placeholder={placeholder}
                    onFocus={handleFocusIn}
                    onBlur={handleFocusOut}
                    autoComplete="off"
                />
                <S.Submit type="submit" aria-label="Submit search">
                    <svg width="18" height="19" xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M17.72632 17.30599l-4.4373-4.70314c1.1409-1.38216 1.76601-3.1212 1.76601-4.9316C15.05503 3.44138 11.67813 0 7.52752 0S0 3.44139 0 7.67124c0 4.22986 3.37691 7.67125 7.52752 7.67125 1.5582 0 3.04308-.47896 4.31261-1.38817l4.47102 4.73883c.18688.19778.43823.30685.70758.30685.25496 0 .49682-.09906.68043-.27917.39012-.38256.40256-1.01694.02716-1.41484zM7.52752 2.00119c3.06795 0 5.56381 2.54352 5.56381 5.67005s-2.49586 5.67005-5.56381 5.67005c-3.06796 0-5.56382-2.54352-5.56382-5.67005S4.45956 2.0012 7.52752 2.0012z"
                            fill="#FFF"
                            fillRule="nonzero"
                        />
                    </svg>
                </S.Submit>
            </S.Wrapper>
        </form>
    );
};

export default SiteSearch;
