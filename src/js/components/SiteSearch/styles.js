/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    transitions
} from '../../globals/variables';

export const Wrapper = styled.div`
    background-color: ${colors.white};
    display: flex;
    padding: ${spacing.s}px;
    transition: ${transitions.bezier};

    ${props =>
        props.hasFocus ? `box-shadow: 0 -10px 40px rgba(0,0,0,.2); transform: scale(1.05)` : ''};
`;

export const Label = styled.label`
    display: block;
    font-size: ${rem(fontSizes.lead)};
    margin-bottom: ${rem(spacing.m)};
    margin-top: ${rem(spacing.m * -1)};
`;

export const Input = styled.input`
    background-color: ${colors.white};
    border: 0;
    color: ${colors.black};
    flex-grow: 1;
    font-size: 16px;
    font-family: ${fontFamilies.bold};
    margin-left: ${spacing.s}px;
    margin-right: ${spacing.s}px;
    outline: none;
    padding: 0;

    &::placeholder {
        color: ${colors.grey};
    }

    @media (min-width: ${rem(breakpoints.mobile)}) {
        font-size: ${rem(fontSizes.lead)};
    }
`;

export const Submit = styled.button`
    align-items: center;
    background-color: ${colors.wewak};
    border: 0;
    cursor: pointer;
    display: flex;
    height: 60px;
    justify-content: center;
    width: 60px;
`;
