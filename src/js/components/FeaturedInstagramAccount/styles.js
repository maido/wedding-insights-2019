/**
 * @prettier
 * @flow
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {responsiveSpacing} from '../../globals/functions';
import {breakpoints, fontFamilies, spacing, themes} from '../../globals/variables';

const AVATAR_SIZE = 115;
const AVATAR_BORDER_SIZE = 2;

export const Container = styled.div`
    align-items: center;
    background-color: ${themes.light.background};
    display: flex;
    font-family: ${fontFamilies.bold};
    padding: ${rem(spacing.m)} ${rem(spacing.m)} ${rem(spacing.m)} ${rem(spacing.s)};

    @media (max-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;

export const Text = styled.div`
    color: ${themes.light.text};
    padding: 0 ${rem(spacing.m)};
`;

export const Account = styled.div`
    direction: flex;
    flex: 0 0 ${rem(AVATAR_SIZE * 1.25)};
    height: 150px;
    justify-content: center;
`;

export const AvatarContainer = styled.div`
    margin: 0 auto;
    position: relative;
    width: ${rem(AVATAR_SIZE)};

    &::after {
        content: '\00a0';
        background: linear-gradient(to bottom, #d82b7e, #f57939);
        border-radius: 100%;
        display: block;
        height: ${rem(AVATAR_SIZE + AVATAR_BORDER_SIZE * 4)};
        left: 0;
        margin-top: ${rem(AVATAR_BORDER_SIZE * 2 * -1)};
        margin-left: ${rem(AVATAR_BORDER_SIZE * 2 * -1)};
        overflow: hidden;
        position: absolute;
        top: 0;
        width: ${rem(AVATAR_SIZE + AVATAR_BORDER_SIZE * 4)};
        z-index: 1;
    }
`;

export const Avatar = styled.img`
    background-color: ${themes.light.background};
    border-radius: 100%;
    height: ${rem(AVATAR_SIZE)};
    width: ${rem(AVATAR_SIZE)};
    padding: ${rem(AVATAR_BORDER_SIZE * 2)};
    position: relative;
    z-index: 2;
`;

export const Username = styled.span`
    color: ${themes.light.accent};
    display: block;
    font-size: ${rem(14)};
    margin-top: ${rem(spacing.s)};
    text-align: center;
    text-transform: uppercase;
`;
