/**
 * @prettier
 * @flow
 */
import * as React from 'react';
import {useTransition, animated} from 'react-spring/web.cjs';
import * as S from './styles';

type Props = {
    accounts: Array<{
        image: string,
        name: string
    }>,
    isActive: boolean,
    text: string
};

const FeaturedInstagramAccount = ({accounts = [], isActive, text}: Props) => {
    const [index, setIndex] = React.useState(0);
    const transitions = useTransition(accounts[index], i => i.name, {
        config: {mass: 3, tension: 150, friction: 30},
        from: {
            opacity: 0,
            position: 'absolute',
            transform: 'translateY(12px)'
        },
        enter: {opacity: 1, transform: 'translateY(0)'},
        leave: {opacity: 0, transform: 'translateY(12px)'}
    });

    React.useEffect(() => {
        let interval;

        if (isActive) {
            interval = setInterval(
                () => setIndex(prevIndex => (prevIndex + 1) % accounts.length),
                2500
            );
        }

        return () => {
            if (isActive) {
                clearInterval(interval);
            }
        };
    }, [isActive]);

    return (
        <S.Container>
            <S.Text>{text}</S.Text>

            {accounts.length > 0 && (
                <S.Account>
                    {transitions.map(({item, props, key}) => (
                        <animated.div key={key} style={props}>
                            <S.AvatarContainer>
                                <S.Avatar src={item.image} aria-hidden="true" />
                            </S.AvatarContainer>
                            <S.Username>{item.name}</S.Username>
                        </animated.div>
                    ))}
                </S.Account>
            )}
        </S.Container>
    );
};

export default FeaturedInstagramAccount;
