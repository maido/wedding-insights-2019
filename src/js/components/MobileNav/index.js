/**
 * @prettier
 * @flow
 */
import React, {useState} from 'react';
import {withTheme} from 'emotion-theming';
import Hamburger from '../Hamburger';
import LystLogo from '../LystLogo';
import * as S from './styles';

type Props = {
    handleLinkClick: Function,
    isActive: boolean,
    navigation: Array<{
        url: string,
        label: string,
        isActive: boolean
    }>,
    theme?: Object
};

const MobileNav = ({handleLinkClick, isActive, navigation, theme = {}}: Props) => {
    const [isHamburgerActive, setIsHamburgerActive] = useState(false);

    const handleHamburgerClick = () => {
        setIsHamburgerActive(state => !state);
    };

    return (
        <S.Container isActive={isActive} theme={theme}>
            <LystLogo />
            <Hamburger isActive={isHamburgerActive} handleClick={handleHamburgerClick} />
            <S.PopoutLinksContainer isActive={isHamburgerActive}>
                {navigation.map(link => (
                    <S.Link
                        href={link.url}
                        key={link.label}
                        isActive={link.isActive}
                        onClick={e => {
                            handleHamburgerClick();
                            handleLinkClick(e, link.url);
                        }}
                    >
                        {link.label}
                    </S.Link>
                ))}
            </S.PopoutLinksContainer>
        </S.Container>
    );
};

export default withTheme(MobileNav);
