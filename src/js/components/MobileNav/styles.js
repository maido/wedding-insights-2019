/**
 * @prettier
 */
import styled from '@emotion/styled';
import {rem} from 'polished';
import {
    breakpoints,
    colors,
    fontFamilies,
    fontSizes,
    spacing,
    themes,
    transitions
} from '../../globals/variables';

export const Container = styled.div`
    background-color: ${props => props.theme && props.theme.background};
    box-shadow: 0 2px 60px 0 rgba(0, 0, 0, 0.15);
    left: 0;
    opacity: 0;
    position: fixed;
    top: 0;
    transform: translateY(${rem(spacing.m * -1)});
    transition: ${transitions.slow};
    width: 100%;
    z-index: 100;

    svg {
        fill: ${props => props.theme && props.theme.accent};
        height: 30px;
        margin: 10px 0 0 ${rem(spacing.m)};
    }

    button {
        margin: 0;
        position: absolute;
        right: ${rem(spacing.m)};
        top: 50%;
        transform: translateY(-50%);
    }
    button > span {
        background-color: ${props => props.theme && props.theme.accent};
    }

    ${props => props.isActive && `opacity: 1; transform: translateY(0);`}
`;

export const Link = styled.a`
    color: ${colors.black};
    display: none;
    font-family: ${fontFamilies.bold};
    font-size: ${fontSizes.lead};

    &:hover,
    &:focus {
        color: ${colors.red};
    }

    & + & {
        margin-left: ${rem(spacing.m)};
    }

    ${props => (props.isActive ? `color: ${colors.black}` : '')};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: inline-block;
    }

    @media (min-width: ${rem(breakpoints.desktop)}) {
        & + & {
            margin-left: ${rem(spacing.l)};
        }
    }
`;

export const PopoutLinksContainer = styled.nav`
    background-color: ${colors.white};
    box-shadow: 0 2px 60px 0 rgba(0, 0, 0, 0.4);
    left: 0;
    opacity: 0;
    padding: ${rem(spacing.m)};
    position: fixed;
    text-align: left;
    top: 0;
    transform: scale(0) translateY(${rem(spacing.m + 18)});
    transform-origin: top right;
    transition: ${transitions.bezier};
    width: 100%;
    z-index: 2;

    a {
        display: block;
        margin-left: 0 !important;
        margin-bottom: ${rem(spacing.xs)};
    }

    ${props => (props.isActive ? 'opacity: 1; transform: scale(1) translateY(0);' : '')};

    @media (min-width: ${rem(breakpoints.mobile)}) {
        display: none;
    }
`;
