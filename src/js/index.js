/**
 * @prettier
 * @flow
 */
import React from 'react';
import {render} from 'react-dom';
import App from './containers/App';

const $app = document.querySelector('#app');
const content = typeof window !== 'undefined' ? window.content : {};

if ($app) {
    render(<App content={content} />, $app);
}

if (module.hot) {
    module.hot.accept();
}
