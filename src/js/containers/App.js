/**
 * @prettier
 * @flow
 */
import React, {Fragment, useEffect, useRef, useState} from 'react';
import {Global} from '@emotion/core';
import {ThemeProvider} from 'emotion-theming';
import kebabCase from 'lodash/kebabCase';
import smoothscroll from 'smoothscroll-polyfill';
import {ParallaxProvider} from 'react-scroll-parallax';
import {StickyContainer, Sticky} from 'react-sticky';
import AnimatedPageBackground from '../components/AnimatedPageBackground';
import ContentSectionFashion from '../components/ContentSectionFashion';
import ContentSectionLifestyle from '../components/ContentSectionLifestyle';
import ContentSectionTrendsetters from '../components/ContentSectionTrendsetters';
import CookieBanner from '../components/CookieBanner';
import FixedSiteNav from '../components/FixedSiteNav';
import LinkBanner from '../components/LinkBanner';
import MobileNav from '../components/MobileNav';
import ObserveScrolling from '../components/ObserveScrolling';
import ScrollToCTA from '../components/ScrollToCTA';
import SiteHeader from '../components/SiteHeader';
import SiteFooter from '../components/SiteFooter';
import * as UI from '../components/UI/styles';
import {styles} from '../globals/styles';
import {themes} from '../globals/variables';

if (typeof window !== 'undefined') {
    smoothscroll.polyfill();
}

type Props = {content: Object};

const App = ({content}: Props) => {
    const [visibleSection, setVisibleSection] = useState(['header', '']);
    const [seenSections, setSeenSections] = useState(['header']);
    const [isScrollingCTAActive, setIsScrollingCTAActive] = useState(true);

    const navigation = content.sections.map(section => ({
        url: `#${kebabCase(section.shortTitle)}`,
        isActive: false,
        label: section.shortTitle,
        theme: section.theme
    }));
    const $navLinkRefs = {};

    navigation.forEach(link => {
        $navLinkRefs[link.url] = useRef(null);
    });

    useEffect(() => {
        const $html = document.querySelector('html');

        if ($html) {
            $html.classList.remove('no-js');
        }
    });

    const handleNavLinkClick = (event, path) => {
        if ($navLinkRefs[path] && $navLinkRefs[path].current) {
            event.preventDefault();
            $navLinkRefs[path].current.scrollIntoView({block: 'start', behavior: 'smooth'});
        }

        if (isScrollingCTAActive) {
            setIsScrollingCTAActive(false);
        }
    };

    const handleScrollingObservation = sectionName => {
        const item = navigation.filter(n => n.label === sectionName);

        if (item.length > 0) {
            setVisibleSection([item[0].label, item[0].theme]);

            setSeenSections(prevSeenSections => {
                if (prevSeenSections.includes(item[0].label)) {
                    return prevSeenSections;
                } else {
                    return [...prevSeenSections, item[0].label];
                }
            });

            if (isScrollingCTAActive) {
                setIsScrollingCTAActive(false);
            }
        } else {
            setVisibleSection(['header', '']);
        }
    };

    const theme =
        visibleSection[1] && themes[visibleSection[1]]
            ? themes[visibleSection[1]]
            : themes.tertiary;

    return (
        <Fragment>
            <Global styles={styles} />
            <ThemeProvider theme={theme}>
                <UI.ShowAt breakpoint="mobile">
                    <FixedSiteNav
                        handleLinkClick={handleNavLinkClick}
                        navigation={navigation}
                        isVisible={visibleSection[0] !== 'header'}
                        visibleSection={visibleSection[0]}
                    />
                </UI.ShowAt>

                <AnimatedPageBackground>
                    <ParallaxProvider>
                        <ObserveScrolling
                            id="header"
                            handleObservation={handleScrollingObservation}
                        >
                            <LinkBanner {...content.header.banner} />
                            <SiteHeader
                                {...content.header.hero}
                                handleLinkClick={handleNavLinkClick}
                                isVisible={visibleSection[0] === 'header'}
                                navigation={navigation}
                            />

                            <UI.ShowAt breakpoint="mobile" style={{position: 'relative'}}>
                                <ScrollToCTA
                                    handleClick={e =>
                                        handleNavLinkClick(
                                            e,
                                            `#${kebabCase(content.sections[0].shortTitle)}`
                                        )
                                    }
                                    isActive={isScrollingCTAActive}
                                />
                            </UI.ShowAt>
                        </ObserveScrolling>

                        <StickyContainer>
                            <Sticky disableCompensation>
                                {({isSticky, style}) => (
                                    <div
                                        style={{
                                            ...style,
                                            position: 'fixed',
                                            top: 0,
                                            width: '100%',
                                            zIndex: 200
                                        }}
                                    >
                                        <UI.HideAt breakpoint="mobile">
                                            <MobileNav
                                                isActive={isSticky}
                                                handleLinkClick={handleNavLinkClick}
                                                navigation={navigation}
                                            />
                                        </UI.HideAt>
                                    </div>
                                )}
                            </Sticky>

                            <ObserveScrolling
                                id={content.sections[0].shortTitle}
                                handleObservation={handleScrollingObservation}
                            >
                                <div
                                    ref={
                                        $navLinkRefs[
                                            `#${kebabCase(content.sections[0].shortTitle)}`
                                        ]
                                    }
                                />
                                <ContentSectionLifestyle
                                    hasBeenSeen={seenSections.includes(
                                        content.sections[0].shortTitle
                                    )}
                                    isVisible={visibleSection[0] === content.sections[0].shortTitle}
                                    section={content.sections[0]}
                                />
                            </ObserveScrolling>

                            <ObserveScrolling
                                id={content.sections[1].shortTitle}
                                handleObservation={handleScrollingObservation}
                            >
                                <div
                                    ref={
                                        $navLinkRefs[
                                            `#${kebabCase(content.sections[1].shortTitle)}`
                                        ]
                                    }
                                />
                                <ContentSectionFashion
                                    hasBeenSeen={seenSections.includes(
                                        content.sections[1].shortTitle
                                    )}
                                    isVisible={visibleSection[0] === content.sections[1].shortTitle}
                                    section={content.sections[1]}
                                />
                            </ObserveScrolling>

                            <ObserveScrolling
                                id={content.sections[2].shortTitle}
                                handleObservation={handleScrollingObservation}
                            >
                                <div
                                    ref={
                                        $navLinkRefs[
                                            `#${kebabCase(content.sections[2].shortTitle)}`
                                        ]
                                    }
                                />
                                <ContentSectionTrendsetters
                                    hasBeenSeen={seenSections.includes(
                                        content.sections[2].shortTitle
                                    )}
                                    isVisible={visibleSection[0] === content.sections[2].shortTitle}
                                    section={content.sections[2]}
                                />
                            </ObserveScrolling>
                        </StickyContainer>
                        <ObserveScrolling
                            id="footer"
                            handleObservation={handleScrollingObservation}
                        >
                            <SiteFooter {...content.footer} />
                        </ObserveScrolling>
                        <CookieBanner {...content.cookieBanner} />
                    </ParallaxProvider>
                </AnimatedPageBackground>
            </ThemeProvider>
        </Fragment>
    );
};

export default App;
