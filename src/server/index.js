/**
 * @prettier
 */
require('dotenv').config();
const path = require('path');
const http = require('http');
const express = require('express');
const React = require('react');
const get = require('lodash/get');
const {renderToString} = require('react-dom/server');
const App = require('../js/containers/App').default;
const content = require(`../content/en.json`);

const app = express();
const IS_PRODUCTION = process.env.NODE_ENV === 'production';

if (!IS_PRODUCTION) {
    /* eslint-disable */
    const webpack = require('webpack');
    const webpackDevMiddleware = require('webpack-dev-middleware');
    const webpackHotMiddleware = require('webpack-hot-middleware');
    const webpackConfig = require('../../webpack.dev.config');
    const webpackCompiler = webpack(webpackConfig);
    /* eslint-enable */

    app.use(
        webpackDevMiddleware(webpackCompiler, {
            filename: webpackConfig.output.filename,
            hot: true,
            noInfo: true,
            publicPath: webpackConfig.output.publicPath
        })
    );
    app.use(webpackHotMiddleware(webpackCompiler));
}

app.set('view engine', 'ejs');
app.use(express.static('../fonts'));
app.use('/static/fonts', express.static(path.join(__dirname, '../fonts')));
app.use('/static/images', express.static(path.join(__dirname, '../images')));
app.use(express.static('../js'));
app.get('/', (req, res) => {
    const meta = get(content, 'meta');
    const heroImage = get(content, 'header.hero.image');
    const appHTML = renderToString(<App content={content} />);
    const jsFiles = ['main.js'];

    res.render('index', {
        appHTML,
        heroImage,
        jsFiles,
        meta,
        content: JSON.stringify(content)
    });
});

const server = http.createServer(app);
const port = 3000;

server.listen(port);
server.on('listening', () => console.log(`🤖 Server running at http://localhost:${port}`));
