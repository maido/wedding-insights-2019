/**
 * @prettier
 */
import fs from 'fs';
import sharp from 'sharp';
import {WIDTHS as widths} from './src/js/components/PreloadedImage';

const init = () => {
    const inputFolder = `./src/raw-content-images/`;
    const outputFolder = `./src/images/`;
    const sections = ['fashion', 'trendsetters'];

    sections.map(section => {
        fs.readdir(`${inputFolder}${section}/`, (err, files = []) => {
            files
                .filter(file => file.charAt(0) !== '.')
                .map(file => {
                    if (file.includes('.jpg')) {
                        console.log(`Preparing: ${section}/preload_${file}`);

                        sharp(`${inputFolder}${section}/${file}`)
                            .jpeg({
                                quality: 80
                            })
                            .resize({width: 520})
                            .blur(100)
                            .toFile(`${outputFolder}content/preload_${file}`)
                            .then(() => console.log(`Generated: content/preload_${file}`));

                        widths.map(width => {
                            console.log(`Preparing: ${section}/${width}_${file}`);

                            sharp(`${inputFolder}${section}/${file}`)
                                .jpeg({
                                    quality: 80
                                })
                                .resize({width})
                                .toFile(`${outputFolder}content/${width}_${file}`)
                                .then(() => console.log(`Generated: content/${width}_${file}`));
                        });
                    } else {
                        console.log(`Preparing: ${section}/${file}`);

                        sharp(`${inputFolder}${section}/${file}`)
                            .png({
                                quality: 80
                            })
                            .toFile(`${outputFolder}content/${file}`)
                            .then(() => console.log(`Generated: content/${file}`));
                    }
                });
        });
    });
};

init();
