/**
 * @prettier
 */
const path = require('path');
const webpack = require('webpack');
const FlowBabelWebpackPlugin = require('flow-babel-webpack-plugin');

module.exports = {
    entry: {
        main: ['webpack-hot-middleware/client', path.resolve(__dirname, 'src/js/index.js')]
    },
    devServer: {
        contentBase: './src'
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.js$/,
                use: 'babel-loader'
            },
            {
                test: /\.(woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        context: '',
                        name: '[name].[ext]',
                        mimetype: 'application/font-woff',
                        outputPath: '../fonts/',
                        publicPath: './'
                    }
                }
            }
        ]
    },
    output: {
        filename: '[name].js',
        path: path.resolve(__dirname, 'dist/static/js'),
        publicPath: '/static/js'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin()
        // new FlowBabelWebpackPlugin()
    ],
    resolve: {
        extensions: ['.js'],
        modules: ['node_modules', path.resolve(__dirname, 'src')]
    },
    mode: 'development'
};
