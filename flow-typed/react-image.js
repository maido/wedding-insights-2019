import * as React from 'react';

declare module 'react-image' {
    declare type Props = {
        src: string | Array<string>,
        container?: React.Node,
        loader?: React.Node,
        unloader?: React.Node,
        decode?: boolean
    };

    declare export default class Image extends React$Component<Props> {}
}
    
