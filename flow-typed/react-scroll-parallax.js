declare module 'react-scroll-parallax' {
    declare type Props = {
        x?: Array<number>,
        y?: Array<number>
    };

    declare export class Parallax extends React$Component<Props> {}
    declare export class ParallaxProvider extends React$Component<Props> {}
}
    
