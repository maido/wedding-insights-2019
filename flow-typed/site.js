type TextBanner = {
    text: string,
    cta: CTA
};

type CTA = {
    label: string,
    url: string
};

type Photo = {
    defaultSize: string,
    height: number,
    isVisible: boolean,
    load: boolean,
    lowres: string,
    hires: string,
    width: number
};

type PageHero = {
    logo: string,
    title: string,
    text: string,
    image: string
};

type ContentSection = {
    items: Array<ContentSectionItem>,
    stat: {
        stat: string,
        label: string
    },
    id: string,
    text: string,
    theme: string,
    title: string
};

type ContentSectionItem = {
    images: Array<Photo>,
    text: string,
    title: string
};
