/**
 * @prettier
 */
const path = require('path');
const webpack = require('webpack');
const FlowBabelWebpackPlugin = require('flow-babel-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    entry: {
        main: path.resolve(__dirname, 'src/js/index.js')
    },
    module: {
        rules: [
            {
                exclude: /node_modules/,
                test: /\.js$/,
                use: 'babel-loader'
            },
            {
                test: /\.(woff|woff2)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        context: '',
                        name: '[name].[ext]',
                        mimetype: 'application/font-woff',
                        outputPath: '../fonts/',
                        publicPath: './'
                    }
                }
            }
        ]
    },
    output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'dist/static/js'),
        publicPath: '/static/js'
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.optimize.ModuleConcatenationPlugin(),
        // new FlowBabelWebpackPlugin(),
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, 'src/images/'),
                to: path.resolve(__dirname, 'dist/static/images/')
            },
            {
                from: path.resolve(__dirname, 'src/fonts/'),
                to: path.resolve(__dirname, 'dist/static/fonts/')
            }
        ])
    ],
    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()]
    },
    resolve: {
        extensions: ['.js'],
        modules: ['node_modules', path.resolve(__dirname, 'src')]
    },
    mode: 'production'
};
